layui.define(['jquery', 'form','table'], function(exports) {
    var $ = layui.jquery;
    var table = layui.table;
    var tableIns ;
    var obj = {
        assign: function(formData) {
            var input = '', form = layui.form;
            for (var i in formData) {
                switch($('.field-'+i).attr('type')) {
                    case 'select':
                        input = $('.field-'+i).find('option[value="'+formData[i]+'"]');
                        input.prop("selected", true);
                        break;
                        
                    case 'radio':
                        input = $('.field-'+i+'[value="'+formData[i]+'"]');
                        input.prop('checked', true);
                        break;

                    case 'checkbox':
                        if (typeof(formData[i]) == 'object') {
                            for(var j in formData[i]) {
                                input = $('.field-'+i+'[value="'+formData[i][j]+'"]');
                                input.prop('checked', true);
                            }
                        } else {
                            input = $('.field-'+i+'[value="'+formData[i]+'"]');
                            input.prop('checked', true);
                        }
                        break;

                    case 'img':
                        if (formData[i]) {
                            input = $('.field-'+i);
                            input.attr('src', formData[i]);
                        }
                        break;

                    default:
                        input = $('.field-'+i);
                        input.val(formData[i]);
                        break;
                }
                
                if (input.attr('data-disabled')) {
                    input.prop('disabled', true);
                }

                if (input.attr('data-readonly')) {
                    input.prop('readonly', true);
                }
            }
            form.render();
        },
        tableRender : function (tableParams) {
            tableParams.defaultToolbar = tableParams.defaultToolbar || ['filter'];


            tableIns = table.render({
                elem: '#lists-table'
                ,height: 'full-30'
                ,url: tableParams.url //数据接口
                ,toolbar: '#headToolbar' //开启头部工具栏，并为其绑定左侧模板
                ,defaultToolbar:tableParams.defaultToolbar
                ,page: true //开启分页
                ,limit: 30
                ,cols: tableParams.cols
                ,parseData: function(res){   //res 即为原始返回的数据
                    return {
                        "code": res.code,         //解析接口状态
                        "msg": res.msg,           //解析提示文本
                        "count": res.data.count,  //解析数据长度
                        "data": res.data.lists    //解析数据列表
                    };
                }
                ,done: function(res, curr, count) {
                    $(".layui-table-main tr").each(function (index, val) {
                        $($(".layui-table-fixed-l .layui-table-body tbody tr")[index]).height($(val).height());
                        $($(".layui-table-fixed-r .layui-table-body tbody tr")[index]).height($(val).height());
                    })
                }
            });
        },
        tableReload : function (whereParams) {

            if ($.isEmptyObject(whereParams)) {
                tableIns.reload()
            } else {
                tableIns.reload({
                    where:whereParams,
                    page : {
                        curr: 1
                    }
                })
            }
        }
    };

    exports('func', obj);
}); 