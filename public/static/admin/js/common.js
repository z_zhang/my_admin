layui.define(['element', 'form', 'table','func'], function(exports) {
    var $ = layui.jquery,element = layui.element,
        layer = layui.layer,
        form = layui.form,
        table = layui.table;

    //新增自己需要的校验类
    form.verify({
            pass: [
                /^[\S]{6,12}$/
                ,'密码必须6到12位，且不能出现空格'
            ]
        });

    //打开一个窗口
    function popIframe(href='',title='')
    {
        var query = '';

        var heights = parseInt( $(window).height() * 2 / 3  )+"px";
        var widths =  parseInt( $(window).width() * 2 / 3 )+"px";


        var def = {width: widths,height:heights, idSync: false, table: 'dataTable', type: 2, url: href, title: title};
        var opt = {};
        opt = Object.assign({}, def, opt);

        if (!def.url) {
            layer.open({
                title:'未设置href参数'
                ,content:'请设置href参数'
                ,icon:2
            });
            return false;
        }

        if ($('.checkbox-ids:checked').length <= 0) {
            var checkStatus = table.checkStatus(opt.table);

            for (var i in checkStatus.data) {
                query += '&id[]=' + checkStatus.data[i].id;
            }
        } else {
            $('.checkbox-ids:checked').each(function() {
                query += '&id[]=' + $(this).val();
            })
        }

        layer.open({type: opt.type, title: opt.title, content: opt.url, area: [opt.width,opt.height]});
    }


    //点击弹窗的
    $(document).on('click', '.save-pop', function() {
        var that = $(this);

        popIframe(that.attr('href'),that.attr('title'));
        return false;
    });

    //监听 添加/编辑提交
    form.on('submit(submit-saved)', function(data){
        var _form = $(data.form),
                    that = $(this);

        // layer.msg(JSON.stringify(data.field));

        var msgOpen =  layer.msg('数据提交中...',{time:500000});
        that.prop('disabled', true);

        $.ajax({
            type: "POST",
            url: _form.attr('action'),
            data: data.field,
            success: function(res) {
                layer.close(msgOpen);

                if (res.data.captcha) {
                    $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                }

                if (res.code == '00000') {

                    layer.msg(res.msg, {time:1500}, function() {

                        var parentFunc = window.parent.layui.func;
                        var parentTableIns = window.parent.layui.func.tableIns;

                        if ( "undefined" == typeof parentTableIns )  {
                            window.parent.location.reload();
                        } else  {
                            parentFunc.tableReload({});
                        }

                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

                        parent.layer.close(index);  //再执行关闭

                    });

                } else {
                    layer.open({
                        title:'哎呀，提交没有通过!'
                        ,content:res.msg
                        ,icon:2
                    });

                    that.prop('disabled', false);
                }

            },
            error: function (XMLHttpRequest) {
                layer.close(msgOpen);

                if ( 422 == XMLHttpRequest.status  ) {
                    var errorMsg = '';

                    $.each(XMLHttpRequest.responseJSON.errors,function (key, value) {
                        errorMsg += (value[0]+"<br />");
                    });

                    layer.open({
                        title:'哎呀，提交没有通过!'
                        ,content:errorMsg
                        ,icon:2
                    })
                }
                that.prop('disabled', false);

            }
        });
        return false;
    });

    //列表页刷新
    $(document).on('click', '.reload', function() {

        layui.func.tableReload({});
    });

    exports('common', {});
});
