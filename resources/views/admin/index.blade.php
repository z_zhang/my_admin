<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{$backstageTitle}}</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css?version={{config('app.html_version')}}">
    <link rel="stylesheet" href="/static/admin/css/common.css?version={{config('app.html_version')}}">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_247300_6w8ov9xat7i.css">
    <link rel="stylesheet" href="/static/admin/fonts/typicons/min.css">
    <link rel="stylesheet" href="/static/admin/fonts/font-awesome/min.css">
    <style>
        #switchNav i.aicon {
            margin-right: 2px;
        }
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="fl admin-logo">
            @if( !empty($logo) )
                <img src="{{$logo}}" alt="" style="width:165px;height: 60px;">
            @else
                {{$backstageTitle}}

            @endif
        </div>
        <div class="fl header-fold"><a href="javascript:;" title="打开/关闭左侧导航" class="layui-icon layui-icon-shrink-right" id="foldSwitch"></a></div>

        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            @foreach ($topMenus as $topMenu)
                <li class="layui-nav-item"><a href="javascript:;" class="top-menu" data-id="menu-{{$topMenu['id']}}">{{ $topMenu['title'] }}</a></li>
            @endforeach
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <i class="layui-icon" style="font-size: 20px; color: #ffb800;">&#xe60c;</i>
                    {{ session('admin')['nick']}}
                </a>
            </li>
            @if( $viewAuthBool['info'] )
                <li class="layui-nav-item"><a href="/my/index" data-id="2"  class="admin-nav-item">个人资料</a></li>
            @endif

            <li class="layui-nav-item"><a href="/logout">退出</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black" id="switchNav">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            @foreach ($twoThreeMenus as $key => $twoThreeMenu)
              @if ( $key != 1 )
              <ul class="layui-nav layui-nav-tree"  lay-filter="test" data-id="menu-{{$key}}" style="display: none"  >
              @else
              <ul class="layui-nav layui-nav-tree"  lay-filter="test" data-id="menu-{{$key}}" >
              @endif
                    @foreach ($twoThreeMenu as $menu)
                                @if ( $firstMenu['pid'] == $menu['id'])
                                <li class="layui-nav-item layui-nav-itemed">
                                @else
                                <li class="layui-nav-item">
                                @endif

                                    <a class="" href="javascript:;">
                                        @if( !empty($menu['icon']) )
                                            <i class="{{$menu['icon']}}"></i>
                                        @else
                                            <i class="fa fa-th-large"></i>

                                        @endif

                                        {{$menu['title']}}
                                    </a>
                                    @if ( count($menu['child']) != 0 )
                                        @foreach ($menu['child'] as $child)
                                        <dl class="layui-nav-child">
                                            @if ( $child['url'] != '/' )
                                                <dd>

                                                    <a href="{{$child['url']}}" data-id="{{$child['id']}}" class="admin-nav-item">&nbsp;&nbsp;
                                                        @if( !empty($child['icon']) )
                                                            <i class="{{$child['icon']}}"></i>
                                                        @else
                                                            <i class="fa fa-th-large"></i>
                                                        @endif
                                                        {{ $child['title']  }}
                                                    </a>
                                                </dd>
                                            @else
                                                <dd>


                                                    <a href="{{$child['url']}}?other=1" data-id="{{$child['id']}}" class="admin-nav-item">&nbsp;&nbsp;
                                                        @if( !empty($child['icon']) )
                                                            <i class="{{$child['icon']}}"></i>
                                                        @else
                                                            <i class="fa fa-th-large"></i>
                                                        @endif
                                                        {{ $child['title']  }}</a>
                                                </dd>
                                            @endif
                                        </dl>
                                        @endforeach
                                    @endif
                                </li>
                    @endforeach
              </ul>
            @endforeach

        </div>
    </div>

    <div class="layui-body" id="switchBody" style="overflow-y: hidden">
        <div class="layui-tab layui-tab-card" lay-filter="hisiTab" lay-allowClose="true">
            <ul class="layui-tab-title">
                <li lay-id="{{$firstMenu['id']}}" class="layui-this">{{ $firstMenu['title']}}</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show" style="padding-left: 10px;">
                    <iframe lay-id="{{$firstMenu['id']}}" src="{{$firstMenu['url']}}?other=1" width="100%" height="100%" frameborder="0" scrolling="yes" class="hs-iframe">

                    </iframe>

                </div>
            </div>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © 个人开发后台
    </div>
</div>
<script src="/static/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use(['jquery', 'element', 'layer'], function() {
        var $ = layui.jquery, element = layui.element, layer = layui.layer;

        $('.layui-tab-content').height($(window).height() - 145);
        var tab = {
            add: function(title, url, id) {
                // $(".layui-tab-content").css("overflow", "auto");
               var heights = $(window).height() - 155;

                element.tabAdd('hisiTab', {
                    title: '<i class="layui-icon">&#xe638;&nbsp;</i>'+title,
                    content: '<iframe width="100%" height="'+heights+'" lay-id="'+id+'" frameborder="0" src="'+url+'" scrolling="yes" style="overflow-y:hidden" class="x-iframe"></iframe>',
                    id: id
            });
            }, change: function(id) {
                element.tabChange('hisiTab', id);
            }
        };

        $('.admin-nav-item').click(function(event) {
            var that = $(this);
            if ($('iframe[src="'+that.attr('href')+'"]')[0]) {
                tab.change(that.attr('data-id'));
                event.stopPropagation();
                return false;
            }

            if (!that.hasClass('top-nav-item')) {
                that.css({color:'#fff'});
            }
            tab.add(that.text(), that.attr('href'), that.attr('data-id'));
            tab.change(that.attr('data-id'));
            event.stopPropagation();
            return false;
        });

        $(document).on('click', '.layui-tab-close', function() {
            $('.layui-nav-child a[data-id="'+$(this).parent('li').attr('lay-id')+'"]').css({color:'rgba(255,255,255,.7)'});
        });


        $(document).on('click','.top-menu',function () {

            $('.layui-nav-tree').hide();
            $(".layui-nav-tree[data-id='"+$(this).attr('data-id')+"']").show();

        })

        /* 打开/关闭左侧导航 */
        $('#foldSwitch').click(function(){
            var that = $(this);

            if ( that.hasClass('layui-icon-shrink-right') ) {
                that.addClass('layui-icon-spread-left').removeClass('layui-icon-shrink-right');
                $('#switchNav').animate({width:'43px'}, 100).removeClass('close');
                $('#switchBody,.layui-footer').animate({left:'43px'}, 100);


            } else  {
                that.addClass('layui-icon-shrink-right').removeClass('layui-icon-spread-left');
                $('#switchNav').animate({width:'200px'}, 100).removeClass('close');
                $('#switchBody,.layui-footer').animate({left:'200px'}, 100);

            }

            // if (!that.hasClass('ai-zhankaicaidan')) {
            //     that.addClass('ai-zhankaicaidan').removeClass('ai-shouqicaidan');
            //     $('#switchNav').animate({width:'43px'}, 100).addClass('close').hover(function() {
            //         if (that.hasClass('ai-zhankaicaidan')) {
            //             $(this).animate({width:'200px'}, 300);
            //             $('#switchNav .fold-mark').removeClass('fold-mark');
            //             $('a[href="'+window.localStorage.getItem("adminNavTag")+'"]').parent('dd').addClass('layui-this').parents('li').addClass('layui-nav-itemed').siblings('li').removeClass('layui-nav-itemed');
            //         }
            //     },function() {
            //         if (that.hasClass('ai-zhankaicaidan')) {
            //             $(this).animate({width:'43px'}, 300);
            //             $('#switchNav .layui-nav-item').addClass('fold-mark').removeClass('layui-nav-itemed');
            //         }
            //     });
            //     $('#switchBody,.footer').animate({left:'43px'}, 100);
            //     $('#switchNav .layui-nav-item').addClass('fold-mark').removeClass('layui-nav-itemed');
            //
            // } else {
            //     $('a[href="'+window.localStorage.getItem("adminNavTag")+'"]').parent('dd').addClass('layui-this').parents('li').addClass('layui-nav-itemed').siblings('li').removeClass('layui-nav-itemed');
            //     that.removeClass('ai-zhankaicaidan').addClass('ai-shouqicaidan');
            //     $('#switchNav').animate({width:'200px'}, 100).removeClass('close');
            //     $('#switchBody,.footer').animate({left:'200px'}, 100);
            //     $('#switchNav .fold-mark').removeClass('fold-mark');
            // }
        });
    });
</script>
</body>
</html>