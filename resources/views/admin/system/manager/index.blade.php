@extends('layouts.admin')


@section('css')
    <style>
        .layui-table-cell {
            /*line-height: 17px !important;*/ //可根据自己的样式来修改
        vertical-align: middle;
            height: auto;
            text-overflow: inherit;
            white-space: normal;
        }
    </style>
@endsection


@section('content')

    <table id="lists-table" lay-filter="table-filter"> </table>

    <script type="text/html" id="headToolbar">
        <div class="layui-btn-container">
            <a class="layui-btn layui-btn-normal layui-btn-sm reload" href="###" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>

        @if ( $viewAuthBool['add'] )   <a class="layui-btn layui-btn-normal layui-btn-sm save-pop" href="/system/admin/add" title="添加管理员"><i class="layui-icon">&#xe608;</i>添加管理员</a> @endif
        </div>
    </script>

    <script type="text/html" id="operateBar">
        @{{# if(d.id > 1 ) {  }}
        @if ( $viewAuthBool['update'] )   <a class="layui-btn layui-btn-xs save-pop" href="/system/admin/update?id=@{{d.id}}" title="编辑管理员">编辑</a> @endif
        @if ( $viewAuthBool['password'] )    <a class="layui-btn layui-btn-xs save-pop" href="/system/admin/password?id=@{{d.id}}" title="修改密码">修改密码</a> @endif
        @{{# } }}
    </script>

    <script type="text/html" id="rolesCols">
        <ul>
         @{{#  layui.each(d.roles, function(index, item){ }}
            <li>@{{ item.name  }} </li>
         @{{#  }); }}
        </ul>
    </script>


        @endsection

@section('script')
    <script>
        layui.use('func', function(){
            var tableParams = {url: '',cols: []};

            tableParams.url = '{{ url()->current() }}';
            tableParams.cols = [[ //表头
                {field: 'id', title: 'ID',  sort: true, width:50}
                ,{field: 'username', title: '账号'}
                ,{field: 'nick', title: '昵称'}
                ,{field: 'mobile', title: '手机号'}
                ,{field: 'email', title: '邮箱'}
                ,{field: '', title: '角色',templet:'#rolesCols'}
                ,{field: 'last_login_ip', title: '最近登录ip'}
                ,{field: 'last_login_time', title: '最近登录时间'}
                ,{field: 'created_at', title: '创建时间'}
                ,{field: 'updated_at', title: '更新时间'}
                ,{fixed: 'right', title:'操作',templet: '#operateBar'}
            ]];

            layui.func.tableRender(tableParams);
        });
    </script>

@endsection


