@extends('layouts.admin')

@section('content')
    <table class="layui-table">
        <colgroup>
            <col width="200">
            <col>
        </colgroup>
        <thead>

        </thead>
        <tbody>
        <tr>
            <td>昵称</td>
            <td>{{ $user->nick  }}</td>
        </tr>
        <tr>
            <td>账号</td>
            <td>{{ $user->username  }}</td>
        </tr>
        <tr>
            <td>角色</td>
            <td>
                @foreach($user->roles as $role)
                {{ $role->name }}<br />
                @endforeach
            </td>
        </tr>
        <tr>
            <td>手机号</td>
            <td>{{ $user->mobile  }}</td>
        </tr>
        <tr>
            <td>邮箱</td>
            <td>{{ $user->email  }}</td>
        </tr>
        <tr>
            <td>最近登录</td>
            <td>{{ $user->last_login_time  }}</td>
        </tr>
        <tr>
            <td>最近登录ip</td>
            <td>{{ $user->last_login_ip  }}</td>
        </tr>

        <tr>
            <td colspan="2">
               @if( $viewAuthBool['update'] ) <button type="button" class="layui-btn layui-btn-sm save-pop" href="/my/update" title="个人编辑"> 编辑信息 </button> @endif
               @if( $viewAuthBool['password'] ) <button type="button" class="layui-btn layui-btn-sm save-pop" href="/my/password" title="密码修改"> 更改密码 </button> @endif
            </td>
        </tr>

        </tbody>
    </table>
@endsection

