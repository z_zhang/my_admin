@extends('layouts.admin')

@section('content')
        <form class="layui-form save-form" action="">

                <div class="layui-form-item">
                        <label class="layui-form-label">昵称</label>
                        <div class="layui-input-block">
                                <input type="text" name="nick" required  lay-verify="required" placeholder="请输入昵称" autocomplete="off" class="layui-input field-nick">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">账号</label>
                        <div class="layui-input-block">
                                <input type="text" name="username" required lay-verify="required" placeholder="请输入账号" autocomplete="off" class="layui-input field-username">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">手机号</label>
                        <div class="layui-input-block">
                                <input type="text" name="mobile" required  lay-verify="required|phone" placeholder="请输入手机号" maxlength="14" autocomplete="off" class="layui-input field-mobile">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">邮箱</label>
                        <div class="layui-input-block">
                                <input type="text" name="email" required  lay-verify="required|email" placeholder="请输入邮箱" autocomplete="off" class="layui-input field-email">
                        </div>
                </div>

                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>
@endsection

@section('script')
<script>
      layui.use('func', function(){
              @if ( isset($info) )
              layui.func.assign( @json($info) );
              @endif
      });
</script>

@endsection
