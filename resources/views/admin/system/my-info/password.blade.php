@extends('layouts.admin')

@section('content')
        <form class="layui-form save-form" action="">

                <div class="layui-form-item">
                        <label class="layui-form-label">原密码</label>
                        <div class="layui-input-inline">
                                <input type="password" name="old_password" required  lay-verify="required|pass"  placeholder="请输入原密码" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">新密码</label>
                        <div class="layui-input-inline">
                                <input type="password" name="password" required  lay-verify="required|pass" placeholder="请输入新密码" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">确认密码</label>
                        <div class="layui-input-inline">
                                <input type="password" name="password_confirmation" required  lay-verify="required|pass" placeholder="请输入确认密码" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>
@endsection

