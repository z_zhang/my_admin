@extends('layouts.admin')

@section('content')
        <form class="layui-form" >

                <div class="layui-form-item">
                <label class="layui-form-label">类型</label>

                        <div class="layui-input-inline">
                                <select name="role_id" lay-verify="required"  class="field-role_id" lay-filter="select-type">
                                    <option value="text">文本</option>
                                    <option value="switch">开关</option>
                                    <option value="array">数组</option>
                                    <option value="map">键值对</option>
                                    <option value="img">图像</option>
                                </select>
                        </div>
                </div>
        </form>

        <form class="layui-form config-type config-text" action="">

                <input type="hidden" name="type"   value="1"  autocomplete="off" class="layui-input">


                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>



                <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">内容</label>
                        <div class="layui-input-block">
                                <textarea name="value" placeholder="请输入内容" class="layui-textarea field-intro"></textarea>
                        </div>
                </div>


                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>

        <form class="layui-form config-type config-switch" action="" style="display: none">

            <input type="hidden" name="type"   value="5"  autocomplete="off" class="layui-input">


            <div class="layui-form-item">
                <label class="layui-form-label">唯一键</label>
                <div class="layui-input-block">
                    <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input">
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">分组</label>
                <div class="layui-input-block">
                    <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                    <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">开关</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="value" lay-skin="switch"  value="1" lay-text="开启|关闭">
                </div>
            </div>



            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="submit-switch">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>


        <form class="layui-form config-type config-array" action="" style="display: none">
                <input type="hidden" name="type"   value="2"  autocomplete="off" class="layui-input">

                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">数组按钮</label>

                        <div class="layui-input-block">
                         <button  class="layui-btn layui-btn-sm btn-array">添加</button>
                         <button  class="layui-btn layui-btn-sm  btn-del-array layui-btn-danger">删除</button>
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">数组值:</label>
                </div>


                <div class="layui-form-item array-value" id="array-value-id-1">
                        <div class="layui-input-block">
                                <input type="text" name="array_value1"  autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item" id="array-submit">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-array">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>

        <form class="layui-form config-type config-map" action="" style="display: none">
                <input type="hidden" name="type"   value="3"  autocomplete="off" class="layui-input">

                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">键值对按钮</label>

                        <div class="layui-input-block">
                                <button  class="layui-btn layui-btn-sm btn-add-map">添加</button>
                                <button  class="layui-btn layui-btn-sm  btn-del-map layui-btn-danger">删除</button>
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">键值对:</label>
                </div>


                <div class="layui-form-item map-value" id="map-value-id-1">
                        <label class="layui-form-label"></label>

                        <div class="layui-input-inline">
                                <input type="text" name="map_name1" autocomplete="off" class="layui-input">
                        </div>

                        <div class="layui-input-inline">
                                <input type="text" name="map_value1" autocomplete="off" class="layui-input">
                        </div>

                </div>


                <div class="layui-form-item" id="map-submit">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-map">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>

        <form class="layui-form config-type config-img" action="" style="display: none">
                <input type="hidden" name="type" value="4" autocomplete="off" class="layui-input">

                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

                <div class="layui-form-item">
                        <div class="layui-input-block">

                                <div class="layui-upload">
                                        <button type="button" class="layui-btn" id="upload">上传图片</button>
                                        <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="uploadImg" style="width: 100px;height: 100px">
                                                <p id="uploadText"></p>
                                                <input type="hidden" name="value" id="uploadInput">
                                        </div>
                                </div>
                        </div>
                </div>


                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-img">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>



@endsection

@section('script')
<script>
        layui.use(['form','jquery','upload'], function(){
            var form = layui.form;
            var $ = layui.jquery;
            var upload = layui.upload;



            // config-type
            form.on('select(select-type)', function(data){
                     $('.config-type').hide();
                     $('.config-'+data.value).show();
            });



            //数组添加
            $(document).on('click','.btn-array',function () {
                let  arrayHtml = '';
                let  arrayNum = $('.array-value').length+1;
                arrayHtml = '<div class="layui-form-item array-value" id="array-value-id-'+arrayNum+'">\n' +
                        '<div class="layui-input-block">\n' +
                        '<input type="text" name="array_value'+arrayNum+'" autocomplete="off" class="layui-input">\n' +
                        '</div>\n' +
                        '</div>';

                $('#array-submit').before(arrayHtml);
                return false;
            });

            //btn-del-array
            $(document).on('click','.btn-del-array',function () {
                let  arrayNum = $('.array-value').length;
                    $("#array-value-id-"+arrayNum).remove();

                return false;
            });


            //map添加
            $(document).on('click','.btn-add-map',function () {
                        let  mapHtml = '';
                        let  mapNum = $('.map-value').length+1;
                        mapHtml = '<div class="layui-form-item map-value" id="map-value-id-'+mapNum+'">\n' +
                                '<label class="layui-form-label"></label>\n' +
                                '\n' +
                                '<div class="layui-input-inline">\n' +
                                '<input type="text" name="map_name'+mapNum+'" autocomplete="off" class="layui-input">\n' +
                                '</div>\n' +
                                '\n' +
                                '<div class="layui-input-inline">\n' +
                                '<input type="text" name="map_value'+mapNum+'" autocomplete="off" class="layui-input">\n' +
                                '</div>\n' +
                                '\n' +
                                '</div>';

                        $('#map-submit').before(mapHtml);
                        return false;
            });

            //map删除
            $(document).on('click','.btn-del-map',function () {
                        let  arrayNum = $('.map-value').length;
                        $("#map-value-id-"+arrayNum).remove();
                        return false;
            });

            //开关的提交
            form.on('submit(submit-switch)', function(data){
                var _form = $(data.form),
                    that = $(this);

                data.field.value =  data.field.value || 0;

                ajaxSumit(_form,that,data.field);
            });

            //数组的提交
            form.on('submit(submit-array)', function(data){
                var _form = $(data.form),
                        that = $(this);

                let  arrayNum = $('.array-value').length;
                let  valueArry = new Array();
                console.log(data.field);

                for (var i = 1; i <= arrayNum; i++) {
                       let arrayValue= $(" input[ name='array_value"+i+"' ] ").val();
                        console.log(arrayValue);
                        valueArry.push(arrayValue)
                }

                // console.log(valueArry);
                data.field.value = JSON.stringify(valueArry);
                // console.log(data.field);

                ajaxSumit(_form,that,data.field);

                return false;
            });

            //map的提交
            form.on('submit(submit-map)', function(data){
                var _form = $(data.form),
                        that = $(this);

                let  arrayNum = $('.map-value').length;
                let  valueObject = {};
                // console.log(data.field);

                for (var i = 1; i <= arrayNum; i++) {
                        let mapKey= $(" input[ name='map_name"+i+"' ] ").val();
                        let mapValue= $(" input[ name='map_value"+i+"' ] ").val();
                        console.log(mapKey+":"+mapValue);
                        valueObject[mapKey] = mapValue;
                }

                // console.log(valueArry);
                data.field.value = JSON.stringify(valueObject);
                // console.log(data.field);

                ajaxSumit(_form,that,data.field);

                return false;
            });

            //图片上传
            var uploadInst = upload.render({
                elem: '#upload'
                ,url: '/upload'
                ,size: 3072
                ,before: function(obj){
                        layer.load(); //上传loading
                }
                ,done: function(res){
                        layer.closeAll('loading'); //关闭loading
                        //如果上传失败
                        if(res.code != '00000'){
                             return layer.msg('上传失败'+res.msg);
                        }
                        //上传成功
                        $('#uploadInput').val(res.data.src);
                        $('#uploadImg').attr('src',res.data.img_src);

                }
                ,error: function(){
                         layer.closeAll('loading'); //关闭loading
                        //演示失败状态，并实现重传
                        var demoText = $('#uploadText');
                        demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                        demoText.find('.demo-reload').on('click', function(){
                            uploadInst.upload();
                        });
                }
            });

            //img的提交
            form.on('submit(submit-img)', function(data){
                var _form = $(data.form),
                        that = $(this);

                ajaxSumit(_form,that,data.field);

                return false;
            });

            function ajaxSumit(_form,that,datas) {
                var msgOpen =  layer.msg('数据提交中...',{time:500000});
                that.prop('disabled', true);

                $.ajax({
                        type: "POST",
                        url: _form.attr('action'),
                        data: datas,
                        success: function(res) {
                                layer.close(msgOpen);

                                if (res.data.captcha) {
                                        $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                                }

                                if (res.code == '00000') {

                                        layer.msg(res.msg, {time:1500}, function() {

                                                var parentFunc = window.parent.layui.func;

                                                if ( "undefined" == typeof parentFunc )  {
                                                        window.parent.location.reload();
                                                } else  {
                                                        parentFunc.tableReload({});
                                                }

                                                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

                                                parent.layer.close(index);  //再执行关闭

                                        });

                                } else {
                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:res.msg
                                                ,icon:2
                                        });

                                        that.prop('disabled', false);
                                }

                        },
                        error: function (XMLHttpRequest) {
                                layer.close(msgOpen);

                                if ( 422 == XMLHttpRequest.status  ) {
                                        var errorMsg = '';

                                        $.each(XMLHttpRequest.responseJSON.errors,function (name, value) {
                                                errorMsg += (value[0]+"<br />");
                                        });

                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:errorMsg
                                                ,icon:2
                                        })
                                }
                                that.prop('disabled', false);

                        }
                });
            }

        });

</script>

@endsection


