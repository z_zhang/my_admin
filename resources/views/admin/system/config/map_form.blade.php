@extends('layouts.admin')

@section('content')


        <form class="layui-form config-type config-array" action="">
                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input field-name">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input field-title">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input field-group">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

            <div class="layui-form-item">
                <label class="layui-form-label">键值对按钮</label>

                <div class="layui-input-block">
                    <button  class="layui-btn layui-btn-sm btn-add-map">添加</button>
                    <button  class="layui-btn layui-btn-sm  btn-del-map layui-btn-danger">删除</button>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">键值对:</label>
            </div>

            @foreach ($info->value as $name => $item)

            <div class="layui-form-item map-value" id="map-value-id-{{$loop->iteration}}">
                <label class="layui-form-label"></label>

                <div class="layui-input-inline">
                    <input type="text" name="map_name{{$loop->iteration}}" autocomplete="off" class="layui-input" value="{{$name}}">
                </div>

                <div class="layui-input-inline">
                    <input type="text" name="map_value{{$loop->iteration}}" autocomplete="off" class="layui-input" value="{{$item}}">
                </div>

            </div>

            @endforeach

                <div class="layui-form-item" id="map-submit">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-map">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>



@endsection

@section('script')
<script>
        layui.use(['form','jquery','func'], function(){
            var form = layui.form;
            var $ = layui.jquery;

            @if ( isset($info) )
              layui.func.assign( @json($info) );
            @endif

            //map添加
            $(document).on('click','.btn-add-map',function () {
                let  mapHtml = '';
                let  mapNum = $('.map-value').length+1;
                mapHtml = '<div class="layui-form-item map-value" id="map-value-id-'+mapNum+'">\n' +
                    '<label class="layui-form-label"></label>\n' +
                    '\n' +
                    '<div class="layui-input-inline">\n' +
                    '<input type="text" name="map_name'+mapNum+'" autocomplete="off" class="layui-input">\n' +
                    '</div>\n' +
                    '\n' +
                    '<div class="layui-input-inline">\n' +
                    '<input type="text" name="map_value'+mapNum+'" autocomplete="off" class="layui-input">\n' +
                    '</div>\n' +
                    '\n' +
                    '</div>';

                $('#map-submit').before(mapHtml);
                return false;
            });

            //map删除
            $(document).on('click','.btn-del-map',function () {
                let  arrayNum = $('.map-value').length;
                $("#map-value-id-"+arrayNum).remove();
                return false;
            });

            //map的提交
            form.on('submit(submit-map)', function(data){
                var _form = $(data.form),
                    that = $(this);

                let  arrayNum = $('.map-value').length;
                let  valueObject = {};
                // console.log(data.field);

                for (var i = 1; i <= arrayNum; i++) {
                    let mapKey= $(" input[ name='map_name"+i+"' ] ").val();
                    let mapValue= $(" input[ name='map_value"+i+"' ] ").val();
                    console.log(mapKey+":"+mapValue);
                    valueObject[mapKey] = mapValue;
                }

                // console.log(valueArry);
                data.field.value = JSON.stringify(valueObject);
                // console.log(data.field);

                ajaxSumit(_form,that,data.field);

                return false;
            });


            function ajaxSumit(_form,that,datas) {
                var msgOpen =  layer.msg('数据提交中...',{time:500000});
                that.prop('disabled', true);

                $.ajax({
                        type: "POST",
                        url: _form.attr('action'),
                        data: datas,
                        success: function(res) {
                                layer.close(msgOpen);

                                if (res.data.captcha) {
                                        $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                                }

                                if (res.code == '00000') {

                                        layer.msg(res.msg, {time:1500}, function() {

                                                var parentFunc = window.parent.layui.func;

                                                if ( "undefined" == typeof parentFunc )  {
                                                        window.parent.location.reload();
                                                } else  {
                                                        parentFunc.tableReload({});
                                                }

                                                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

                                                parent.layer.close(index);  //再执行关闭

                                        });

                                } else {
                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:res.msg
                                                ,icon:2
                                        });

                                        that.prop('disabled', false);
                                }

                        },
                        error: function (XMLHttpRequest) {
                                layer.close(msgOpen);

                                if ( 422 == XMLHttpRequest.status  ) {
                                        var errorMsg = '';

                                        $.each(XMLHttpRequest.responseJSON.errors,function (name, value) {
                                                errorMsg += (value[0]+"<br />");
                                        });

                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:errorMsg
                                                ,icon:2
                                        })
                                }
                                that.prop('disabled', false);

                        }
                });
            }

        });

</script>

@endsection


