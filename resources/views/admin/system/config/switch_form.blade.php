@extends('layouts.admin')

@section('content')

        <form class="layui-form config-type config-text" action="">


                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input field-name">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input field-title">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input field-group">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>



                <div class="layui-form-item">
                        <label class="layui-form-label">开关</label>
                        <div class="layui-input-block">
                                @if( $info->value == 1 )
                                <input type="checkbox" name="value" lay-skin="switch"  value="1" lay-text="开启|关闭" checked>
                                @else

                                <input type="checkbox" name="value" lay-skin="switch"  value="1" lay-text="开启|关闭">
                                @endif
                        </div>
                </div>


                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-switch">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>


@endsection

@section('script')
<script>
        layui.use(['form','jquery','func'], function(){
                var form = layui.form;

                var $ = layui.jquery;

                @if ( isset($info) )
                layui.func.assign( @json($info) );
                @endif

                //开关的提交
                form.on('submit(submit-switch)', function(data){
                        var _form = $(data.form),
                                that = $(this);

                        data.field.value =  data.field.value || 0;

                        ajaxSumit(_form,that,data.field);
                });

                function ajaxSumit(_form,that,datas) {
                        var msgOpen =  layer.msg('数据提交中...',{time:500000});
                        that.prop('disabled', true);

                        $.ajax({
                                type: "POST",
                                url: _form.attr('action'),
                                data: datas,
                                success: function(res) {
                                        layer.close(msgOpen);

                                        if (res.data.captcha) {
                                                $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                                        }

                                        if (res.code == '00000') {

                                                layer.msg(res.msg, {time:1500}, function() {

                                                        var parentFunc = window.parent.layui.func;

                                                        if ( "undefined" == typeof parentFunc )  {
                                                                window.parent.location.reload();
                                                        } else  {
                                                                parentFunc.tableReload({});
                                                        }

                                                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

                                                        parent.layer.close(index);  //再执行关闭

                                                });

                                        } else {
                                                layer.open({
                                                        title:'哎呀，提交没有通过!'
                                                        ,content:res.msg
                                                        ,icon:2
                                                });

                                                that.prop('disabled', false);
                                        }

                                },
                                error: function (XMLHttpRequest) {
                                        layer.close(msgOpen);

                                        if ( 422 == XMLHttpRequest.status  ) {
                                                var errorMsg = '';

                                                $.each(XMLHttpRequest.responseJSON.errors,function (name, value) {
                                                        errorMsg += (value[0]+"<br />");
                                                });

                                                layer.open({
                                                        title:'哎呀，提交没有通过!'
                                                        ,content:errorMsg
                                                        ,icon:2
                                                })
                                        }
                                        that.prop('disabled', false);

                                }
                        });
                }

        });
</script>

@endsection


