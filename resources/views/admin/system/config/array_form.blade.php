@extends('layouts.admin')

@section('content')


        <form class="layui-form config-type config-array" action="" >
                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input field-name">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input field-title">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input field-group">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">数组按钮</label>

                        <div class="layui-input-block">
                         <button  class="layui-btn layui-btn-sm btn-array">添加</button>
                         <button  class="layui-btn layui-btn-sm  btn-del-array layui-btn-danger">删除</button>
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">数组值:</label>
                </div>

                @foreach ($info->value as $name => $item)
                <div class="layui-form-item array-value" id="array-value-id-{{$name+1}}">
                        <div class="layui-input-block">
                                <input type="text" name="array_value{{$name+1}}"  autocomplete="off" class="layui-input" value="{{$item}}">
                        </div>
                </div>
                @endforeach

                <div class="layui-form-item" id="array-submit">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-array">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>



@endsection

@section('script')
<script>
        layui.use(['form','jquery','func'], function(){
            var form = layui.form;
            var $ = layui.jquery;

            @if ( isset($info) )
              layui.func.assign( @json($info) );
            @endif

            //数组添加
            $(document).on('click','.btn-array',function () {
                let  arrayHtml = '';
                let  arrayNum = $('.array-value').length+1;
                arrayHtml = '<div class="layui-form-item array-value" id="array-value-id-'+arrayNum+'">\n' +
                        '<div class="layui-input-block">\n' +
                        '<input type="text" name="array_value'+arrayNum+'" autocomplete="off" class="layui-input">\n' +
                        '</div>\n' +
                        '</div>';

                $('#array-submit').before(arrayHtml);
                return false;
            });

            //btn-del-array
            $(document).on('click','.btn-del-array',function () {
                let  arrayNum = $('.array-value').length;
                    $("#array-value-id-"+arrayNum).remove();

                return false;
            });



            //数组的提交
            form.on('submit(submit-array)', function(data){
                var _form = $(data.form),
                        that = $(this);

                let  arrayNum = $('.array-value').length;
                let  valueArry = new Array();
                console.log(data.field);

                for (var i = 1; i <= arrayNum; i++) {
                       let arrayValue= $(" input[ name='array_value"+i+"' ] ").val();
                        console.log(arrayValue);
                        valueArry.push(arrayValue)
                }

                // console.log(valueArry);
                data.field.value = JSON.stringify(valueArry);
                // console.log(data.field);

                ajaxSumit(_form,that,data.field);

                return false;
            });


            function ajaxSumit(_form,that,datas) {
                var msgOpen =  layer.msg('数据提交中...',{time:500000});
                that.prop('disabled', true);

                $.ajax({
                        type: "POST",
                        url: _form.attr('action'),
                        data: datas,
                        success: function(res) {
                                layer.close(msgOpen);

                                if (res.data.captcha) {
                                        $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                                }

                                if (res.code == '00000') {

                                        layer.msg(res.msg, {time:1500}, function() {

                                                var parentFunc = window.parent.layui.func;

                                                if ( "undefined" == typeof parentFunc )  {
                                                        window.parent.location.reload();
                                                } else  {
                                                        parentFunc.tableReload({});
                                                }

                                                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

                                                parent.layer.close(index);  //再执行关闭

                                        });

                                } else {
                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:res.msg
                                                ,icon:2
                                        });

                                        that.prop('disabled', false);
                                }

                        },
                        error: function (XMLHttpRequest) {
                                layer.close(msgOpen);

                                if ( 422 == XMLHttpRequest.status  ) {
                                        var errorMsg = '';

                                        $.each(XMLHttpRequest.responseJSON.errors,function (name, value) {
                                                errorMsg += (value[0]+"<br />");
                                        });

                                        layer.open({
                                                title:'哎呀，提交没有通过!'
                                                ,content:errorMsg
                                                ,icon:2
                                        })
                                }
                                that.prop('disabled', false);

                        }
                });
            }

        });

</script>

@endsection


