@extends('layouts.admin')

@section('content')

        <form class="layui-form config-type config-text" action="">


                <div class="layui-form-item">
                        <label class="layui-form-label">唯一键</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入唯一键" autocomplete="off" class="layui-input field-name">
                        </div>
                </div>


                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input field-title">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">分组</label>
                        <div class="layui-input-block">
                                <input type="text" name="group"  placeholder="请输入分组" autocomplete="off" class="layui-input field-group">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>



                <div class="layui-form-item">
                        <div class="layui-input-block">

                                <div class="layui-upload">
                                        <button type="button" class="layui-btn" id="upload">上传图片</button>
                                        <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="uploadImg" style="width: 100px;height: 100px" src="{{$info->img_src}}">
                                                <p id="uploadText"></p>
                                                <input type="hidden" name="value" id="uploadInput" class="field-value">
                                        </div>
                                </div>
                        </div>
                </div>


                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>


@endsection

@section('script')
<script>
        layui.use(['func','upload'], function(){
                var $ = layui.jquery;
                var upload = layui.upload;

                @if ( isset($info) )
                layui.func.assign( @json($info) );
                @endif

                //图片上传
                var uploadInst = upload.render({
                        elem: '#upload'
                        ,url: '/upload'
                        ,size: 3072
                        ,before: function(obj){
                                layer.load(); //上传loading
                        }
                        ,done: function(res){
                                layer.closeAll('loading'); //关闭loading
                                //如果上传失败
                                if(res.code != '00000'){
                                        return layer.msg('上传失败'+res.msg);
                                }
                                //上传成功
                                $('#uploadInput').val(res.data.src);
                                $('#uploadImg').attr('src',res.data.img_src);

                        }
                        ,error: function(){
                                layer.closeAll('loading'); //关闭loading
                                //演示失败状态，并实现重传
                                var demoText = $('#uploadText');
                                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                                demoText.find('.demo-reload').on('click', function(){
                                        uploadInst.upload();
                                });
                        }
                });
        });
</script>

@endsection


