@extends('layouts.admin')

@section('content')

<table id="lists-table" lay-filter="table-filter"> </table>

<script type="text/html" id="headToolbar">
    <div class="layui-btn-container">
        <a class="layui-btn layui-btn-normal layui-btn-sm reload" href="###" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>
        @if ( $viewAuthBool['add'] )
            <a class="layui-btn layui-btn-normal layui-btn-sm save-pop" href="/system/config/add" title="添加"><i class="layui-icon">&#xe608;</i>添加</a>
        @endif
    </div>
</script>

<script type="text/html" id="operateBar">
    @if ( $viewAuthBool['update'] )
        <a class="layui-btn layui-btn-xs save-pop" href="/system/config/update?id=@{{d.id}}" title="编辑">编辑</a>
    @endif
</script>


<script type="text/html" id="operateValue">

    @{{#  if(d.type == 4 ){  }}
        <a href="@{{ d.img_src }}" target="_blank"> <img src="@{{ d.img_src }}" alt="" style="height: 100px; width: 100px;"> </a>
    @{{#  } else if (d.type == 5 ){  }}

            @{{#  if(d.value == 1 ){  }}
                 开
            @{{#  } else { }}
                 关
            @{{#  } }}

    @{{#  } else { }}
        @{{d.value}}
    @{{#  } }}

</script>

@endsection

@section('script')
    <script>
        layui.use('func', function(){
            var tableParams = {url: '',cols: []};
            var $ = layui.jquery;
            //ssa
            tableParams.url = '{{ url()->current() }}';
            tableParams.cols = [[ //表头
                {field: 'id', title: 'ID',  sort: true, fixed: 'left'}
                ,{field: 'name', title: '唯一键'}
                ,{field: 'title', title: '标题'}
                ,{field: 'group', title: '组合'}
                ,{field: 'value', title: '值',templet: '#operateValue'}
                ,{field: 'type_text', title: '类型'}
                ,{field: 'status_text', title: '状态'}
                ,{field: 'updated_at', title: '更新时间'}
                ,{fixed: 'right', title:'操作',templet: '#operateBar'}
            ]];

            layui.func.tableRender(tableParams);

        });
    </script>

@endsection


