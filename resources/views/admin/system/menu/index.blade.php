@extends('layouts.admin')

@section('content')

    <div style="margin: 0 0 10px 10px;">

    @if ( $viewAuthBool['add'] ) <a href="/system/menu/add?pid=0" class="layui-btn layui-btn-sm  save-pop">添加菜单</a> @endif
    </div>

    <div id="menu"></div>
    <br>
    <br>
    <br>

@endsection

@section('script')
    <script>
        layui.use('mytree', function() {
            var $ = layui.jquery;
            var tree = layui.mytree;
            var lists = @json($lists,JSON_PRETTY_PRINT);

            var addUrl = '/system/menu/add';
            var updateUrl = '/system/menu/update';
            // var deleteUrl = 'system/menu/del';
            var editArr = [];

            @if ( $viewAuthBool['add'] )
                editArr.push('add');
            @endif

            @if ( $viewAuthBool['update'] )
                 editArr.push('update');
            @endif

            tree.render({
                elem: '#menu'
                , data: lists
                , id: 'menuId'
                , edit: editArr

            });

            tree.reload('menuId',{
                operate: function(obj){
                    var type = obj.type; //得到操作类型：add、edit、del
                    var data = obj.data; //得到当前节点的数据
                    // var elem = obj.elem; //得到当前节点元素

                    var heights = parseInt( $(window).height() * 2 / 3 )+"px";
                    var widths =  parseInt( $(window).width() * 2 / 3 )+"px";

                    var opt = {width: widths,height:heights, idSync: false,  type: 2, url: '', title: ''};

                    //Ajax 操作
                    var id = data.id; //得到节点索引

                    if(type === 'add'){ //增加节点

                        opt.url= addUrl+'?pid='+id;
                        opt.title= '添加菜单';

                        layer.open({type: opt.type, title: opt.title, content: opt.url, area: [opt.width,opt.height]});

                    } else if(type === 'update'){ //修改节点

                        opt.url= updateUrl+'?id='+id;
                        opt.title= '编辑菜单';

                        layer.open({type: opt.type, title: opt.title, content: opt.url, area: [opt.width,opt.height]});
                        return false;
                    }

                }
            })

        });
    </script>

@endsection