@extends('layouts.admin')
@section('css')
<link rel="stylesheet" href="http://at.alicdn.com/t/font_247300_6w8ov9xat7i.css">
<link rel="stylesheet" href="/static/admin/fonts/typicons/min.css">
<link rel="stylesheet" href="/static/admin/fonts/font-awesome/min.css">
<style>
        #form-icon-preview {
                float: left;
                width: 34px;
                height: 36px;
                line-height: 36px;
                font-size: 30px!important;
                border: 1px solid #e6e6e6;
                text-align: center;
                border-radius: 3px;
        }
</style>
@endsection

@section('content')
        <form class="layui-form save-form" action="">

                <div class="layui-form-item">
                        <label class="layui-form-label">上级</label>
                        <div class="layui-input-block">
                                <select name="pid" lay-verify="required">
                                        <option value="0">顶级</option>
                                        {!! $menusOptions !!}
                                </select>
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">模块</label>
                        <div class="layui-input-inline">
                                <input type="text" name="module" required  lay-verify="required" placeholder="请输入模块英文名称" autocomplete="off" class="layui-input field-module">
                        </div>
                        <div class="layui-form-mid layui-word-aux">诸如system,common</div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-block">
                                <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input field-title">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">路由</label>
                        <div class="layui-input-inline">
                                <input type="text" name="url" required  lay-verify="required" placeholder="请输入url" autocomplete="off" class="layui-input field-url">
                        </div>
                        <div class="layui-form-mid layui-word-aux">要确保唯一性</div>

                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">图标设置</label>
                        <div class="layui-input-inline">
                                <input type="hidden" class="layui-input field-icon" id="input-icon" name="icon" lay-verify="" autocomplete="off" placeholder="">
                        </div>
                        <i class="@if( isset($info) ) {{$info->icon}} @endif" id="form-icon-preview"></i>

                        <a href="/system/menu/icon" title="选择图标" class="layui-btn layui-btn-primary j-iframe-pop fl" style="margin-left: 5px;">选择图标</a>

                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                                <input type="number" name="sort" required  lay-verify="required" placeholder="请输入数字" autocomplete="off" value="1" class="layui-input field-sort">
                        </div>
                        <div class="layui-form-mid layui-word-aux">从小排到大(数字)</div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">系统菜单</label>
                        <div class="layui-input-inline">
                             <input type="radio" name="system" value="1" title="是" class="field-system">
                             <input type="radio" name="system" value="2" title="否" class="field-system" checked >
                        </div>
                        <div class="layui-form-mid layui-word-aux">是 就不可删除</div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">菜单显示</label>
                        <div class="layui-input-inline">
                                <input type="radio" name="nav" value="1" title="显示" class="field-nav" checked>
                                <input type="radio" name="nav" value="2" title="不显示" class="field-nav" >
                        </div>
                </div>

                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>

        </form>
@endsection

@section('script')
<script>
      layui.use(['func','form'], function(){
              var form = layui.form,layer = layui.layer,$ = layui.jquery;


              @if ( isset($info) )
              layui.func.assign( @json($info) );
              @endif

              $('.j-iframe-pop').click(function () {
                      var that = $(this);
                      layer.open({type: 2, title: '图标', content: that.attr('href'), area:['600px','400px'] });

                      return false;
              });

      });
</script>

@endsection
