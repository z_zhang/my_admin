@extends('layouts.admin')

@section('css')
    <link rel="stylesheet" href="/static/admin/css/search.css?version={{config('app.html_version')}}">
@endsection

@section('content')

    <script type="text/html" id="headToolbar">
        <div class="layui-btn-container">
            <a class="layui-btn layui-btn-normal layui-btn-sm reload" href="###" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>
        </div>
    </script>

<div class="search-class">

    <form class="layui-form save-form" action="">

        <div class="layui-form-item">
            <label class="layui-form-label search-label">菜单</label>
            <div class="layui-input-inline search-input">
                <select name="menu_id" class="search-label">
                    <option value="">可选菜单</option>
                        {!! $menus !!}
                </select>
            </div>
            <label class="layui-form-label search-label">管理员</label>
            <div class="layui-input-inline search-input">
                <select name="admin_id" >
                    <option value="">可选管理员</option>
                    @foreach ($admins as  $admin)
                        <option value="{{$admin['id']}}">{{$admin['nick']}}</option>
                    @endforeach

                </select>
            </div>
            <button class="layui-btn  layui-btn-sm" lay-submit lay-filter="search-btn">搜索</button>

        </div>

    </form>

</div>

<table id="lists-table" lay-filter="table-filter"> </table>


@endsection

@section('script')
    <script>
        layui.use('func', function(){
            var tableParams = {url: '',cols: []};
            var $ = layui.jquery;
            var form = layui.form;

                tableParams.url = '{{ url()->current() }}';
            tableParams.cols = [[ //表头
                {field: 'id', title: 'ID',  sort: true, fixed: 'left'}
                ,{field: 'nick', title: '昵称'}
                // ,{field: 'role', title: '角色'}
                ,{field: 'url', title: '路由'}
                ,{field: 'title', title: '路由名称'}
                ,{field: 'method', title: 'http方法'}
                ,{field: 'params', title: '参数'}
                ,{field: 'operate_time', title: '操作时间'}
                ,{field: 'created_at', title: '创建时间'}
            ]];

            layui.func.tableRender(tableParams);


            form.on('submit(search-btn)', function(data) {

                layui.func.tableReload(data.field);
                return false;

            })

        });
    </script>

@endsection


