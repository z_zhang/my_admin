@extends('layouts.admin')

@section('content')

<table id="lists-table" lay-filter="table-filter"> </table>

<script type="text/html" id="headToolbar">
    <div class="layui-btn-container">
        <a class="layui-btn layui-btn-normal layui-btn-sm reload" href="###" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>
        @if ( $viewAuthBool['add'] )  <a class="layui-btn layui-btn-normal layui-btn-sm save-pop" href="/system/role/add" title="添加角色"><i class="layui-icon">&#xe608;</i>添加角色</a> @endif
    </div>
</script>

<script type="text/html" id="operateBar">
    @{{# if(d.id > 1 ) {  }}
    @if ( $viewAuthBool['update'] ) <a class="layui-btn layui-btn-xs save-pop" href="/system/role/update?id=@{{d.id}}" title="编辑角色">编辑</a> @endif
    @if ( $viewAuthBool['menu'] )  <a class="layui-btn layui-btn-xs save-pop" href="/system/role/menu?id=@{{d.id}}" title="权限控制">权限控制</a> @endif
    @{{# } }}
</script>

@endsection

@section('script')
    <script>
        layui.use('func', function(){
            var tableParams = {url: '',cols: []};
            var $ = layui.jquery;

            tableParams.url = '{{ url()->current() }}';
            tableParams.cols = [[ //表头
                {field: 'id', title: 'ID',  sort: true, fixed: 'left'}
                ,{field: 'name', title: '角色名称'}
                ,{field: 'intro', title: '简介'}
                ,{field: 'status_text', title: '状态'}
                ,{field: 'created_at', title: '创建时间'}
                ,{field: 'updated_at', title: '更新时间'}
                ,{fixed: 'right', title:'操作',templet: '#operateBar'}
            ]];

            layui.func.tableRender(tableParams);

        });
    </script>

@endsection


