@extends('layouts.admin')


@section('content')

    <form class="layui-form save-form" action="">

        <div class="layui-form-item">
            <label class="layui-form-label">选择权限</label>
            <div class="layui-input-block">
                <div id="LAY-auth-tree-index"></div>
            </div>
        </div>

        <br />

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </form>

@endsection

@section('script')
    <script>
        layui.config({
            // base: '/static/layui_ext/',
        }).extend({
            authtree: 'authtree',
        });

        layui.use(['jquery', 'authtree', 'form', 'layer'], function(){
            var $ = layui.jquery;
            var authtree = layui.authtree;
            var form = layui.form;
            var layer = layui.layer;
            var trees = @json($menus);

            authtree.render('#LAY-auth-tree-index', trees, {

                layfilter: 'lay-check-auth',
                autowidth: true,
                autoclose: false,
                openall: true,
                theme: 'auth-skin-default',
                themePath: '/static/admin/js/tree_themes/'
            });

        });
    </script>

@endsection