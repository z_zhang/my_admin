@extends('layouts.admin')

@section('content')
        <form class="layui-form save-form" action="">

                <div class="layui-form-item">
                        <label class="layui-form-label">名称</label>
                        <div class="layui-input-block">
                                <input type="text" name="name" required  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input field-name">
                        </div>
                </div>

                <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                                <input type="radio" name="status" value="1" class="field-status" title="可用" checked>
                                <input type="radio" name="status" value="2" class="field-status" title="不可用" >
                        </div>
                </div>

                <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">简介</label>
                        <div class="layui-input-block">
                                <textarea name="intro" placeholder="请输入内容" class="layui-textarea field-intro"></textarea>
                        </div>
                </div>

                <textarea name="auth" placeholder="" class="layui-textarea field-auth" style="display: none">[]</textarea>

                <div class="layui-form-item">
                        <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="submit-saved">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                </div>
        </form>
@endsection

@section('script')
<script>
      layui.use('func', function(){
              @if ( isset($info) )
              layui.func.assign( @json($info) );
              @endif
      });
</script>

@endsection
