<!DOCTYPE html>
<html>
<head>
    <title>后台管理登录</title>
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <style type="text/css">
         /*body{background:radial-gradient(200% 100% at bottom center,#0070aa,#0b2570,#000035,#000);background:radial-gradient(220% 105% at top center,#000 10%,#000035 40%,#0b2570 65%,#0070aa);background-attachment:fixed;overflow:hidden}*/
         body {
             background: #666;
         }
        @keyframes rotate{0%{transform:perspective(400px) rotateZ(20deg) rotateX(-40deg) rotateY(0)}100%{transform:perspective(400px) rotateZ(20deg) rotateX(-40deg) rotateY(-360deg)}}

        .login-box{padding:15px 30px;border:1px solid #aaa;border-radius:5px;background-color:rgba(255,255,255, 0.2);width:300px;position:fixed;left:50%;top:50%;z-index:999;margin:-200px 0 0 -160px;}
        .layui-form-pane .layui-form-label{width:50px;background-color:rgba(255,255,255, 0.5);color:#fff;}
        .layui-form-pane .layui-input-block{margin-left:50px;}
        .login-box .layui-input{font-size:15px;font-weight:400;background-color: #ffffff;display:inline-block;}

        .login-box input[name="password"]{letter-spacing:5px;font-weight:800}
        .login-box input[type="submit"]{letter-spacing:5px;}
        .login-box input[name="captcha"]{width:75px;}
        .captcha{float:right;border-radius:3px;width:120px;height:38px;overflow:hidden;}
        .login-box .layui-btn{width:100%;}
        .login-box .copyright{text-align:center;height:50px;line-height:50px;font-size:12px;color:#aaa}
        .login-box .copyright a{color:#aaa;}
        @media only screen and (min-width:750px){
            .login-box{width:400px;margin:-200px 0 0 -225px!important}
            .login-box input[name="captcha"]{width:165px;}
        }
    </style>
</head>
<body>
<div id="particles-js">
    <div class="login-box">
        <form action="" method="post" class="layui-form layui-form-pane">
            <fieldset class="layui-elem-field layui-field-title">
                <legend style="color:#fff;">管理后台登录</legend>
            </fieldset>
            <div class="layui-form-item">
                <label class="layui-form-label"><i class="layui-icon layui-icon-username"></i></label>
                <div class="layui-input-block">
                    <input type="text" name="username" class="layui-input" lay-verify="required"  placeholder="账号/手机/邮箱" autofocus="autofocus" />
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><i class="layui-icon layui-icon-password"></i></label>
                <div class="layui-input-block">
                    <input type="password" name="password" class="layui-input" lay-verify="required"  placeholder="登录密码" />
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label"><i class="layui-icon layui-icon-vercode"></i></label>
                <div class="layui-input-inline">
                    <input type="text" name="captcha" class="layui-input" placeholder="验证码" /><a href="javascript:;" class="captcha"></a>

                </div>
                <div class="">
                    <img class="thumbnail captcha mt-3 mb-2" src="{{ captcha_src('flat') }}" onclick="this.src='/captcha/flat?'+Math.random()" id="img-code"  title="点击图片重新获取验证码">
                </div>
            </div>


    <input type="submit" value="登录" lay-submit="" lay-filter="formLogin" class="layui-btn layui-btn-normal">
    </form>

    </div>
</div>

<script src="/static/layui/layui.js"></script>

<script type="text/javascript">
    window.sessionStorage.clear();

    layui.use(['form', 'layer', 'jquery'], function() {
        var $ = layui.jquery, layer = layui.layer, form = layui.form, captchaUrl = '{:captcha_src()}';
        form.on('submit(formLogin)', function(data) {
            var that = $(this), _form = that.parents('form'),
                account = $('input[name="username"]').val(),
                pwd = $('input[name="password"]').val(),
                token = $('input[name="__token__"]').val(),
                captcha = $('input[name="captcha"]').val();

            var msgOpen =  layer.msg('数据提交中...',{time:500000});
            that.prop('disabled', true);

            $.ajax({
                type: "POST",
                url: _form.attr('action'),
                data: {'username': account, 'password': pwd, '__token__' : token, captcha: captcha},
                success: function(res) {
                    layer.close(msgOpen);

                    if (res.data.captcha) {
                        $('#captchaImg').attr('src', res.data.captcha).parents('.layui-form-item').show();
                    }

                    // if (res.data.token) {
                    //     $('input[name="__token__"]').val(res.data.token);
                    // }

                    layer.msg(res.msg, {time:1500}, function() {

                        if (res.code == '00000') {
                            location.href = '/';
                        } else {
                            that.prop('disabled', false);
                            $('#img-code').attr("src","/captcha/flat?"+Math.random())

                        }

                    });
                },
                error: function (XMLHttpRequest) {
                    layer.close(msgOpen);

                    if ( 422 == XMLHttpRequest.status  ) {
                        var errorMsg = '';

                        $.each(XMLHttpRequest.responseJSON.errors,function (key, value) {
                            errorMsg += (value[0]+"<br />");
                        });

                        layer.open({
                            title:'哎呀，提交没有通过!'
                            ,content:errorMsg
                            ,icon:2
                        })
                        $('#img-code').attr("src","/captcha/flat?"+Math.random())

                    }
                    that.prop('disabled', false);

                }
            });
            return false;
        });

        $(document).on('click', '#captchaImg', function(){



            $(this).attr('src', captchaUrl+'#rand='+Math.random());
        });
    });

</script>
</body>
</html>