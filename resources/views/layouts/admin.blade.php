<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>后台管理</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css?version={{config('app.html_version')}}">
    <link rel="stylesheet" href="/static/admin/css/common.css?version={{config('app.html_version')}}">
    @section('css')

    @show
</head>
<body>

    <div class="container">
        @yield('content')
    </div>

<script src="/static/layui/layui.js?version={{config('app.html_version')}}"></script>
<script>
    layui.config({
        base: '/static/admin/js/' //你存放新模块的目录，注意，不是layui的模块目录
        ,version:'{{config('app.html_version')}}'
    }).use('common');
</script>
@section('script')

@show

</body>
</html>