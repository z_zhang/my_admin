<?php
/**
 * Created by PhpStorm.
 * User: niebangheng
* Date: 2018/1/17
* Time: 15:29
*/

return [

    'attributes' => [
        'username' => '账号',
        'password' => '密码',
        'nick' => '昵称',
        'old_password' => '旧密码',
        'url' => '路由',
        'name' => '名称',
        'intro' => '简介',
        'status' => '状态',
        'mobile'=> '手机',
        'email' => '邮箱',

    ],


];