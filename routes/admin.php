<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::domain(\Illuminate\Support\Facades\Config::get('app.admin_domain'))->group(function () {

    Route::match(['get', 'post'],'login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');    // 退出

    Route::middleware(['admin.auth'])->group(function () {

        Route::get('/', 'IndexController@index');           //首页，属于个人模块
        Route::post('upload','UploadController@index')->middleware('throttle:30,1');     // 上传文件

        //个人模块
        Route::prefix('my')->group(function (){
            Route::get('/index', 'System\MyInfoController@index');  //个人信息
            Route::match(['get', 'post'],'/update', 'System\MyInfoController@update');       //编辑个人信息
            Route::match(['get', 'post'],'/password', 'System\MyInfoController@password');   //更改密码
        });

        //系统模块
        Route::prefix('system')->group(function (){

            Route::prefix('menu')->group(function (){
                //菜单
                Route::get('/index', 'System\MenuController@index');             //菜单列表
                Route::match(['get', 'post'],'/add', 'System\MenuController@add');     //菜单添加
                Route::match(['get', 'post'],'/update', 'System\MenuController@update');  //菜单编辑

            });

            Route::prefix('role')->group(function (){
                //角色
                Route::get('/index', 'System\RoleController@index');             //角色列表
                Route::match(['get', 'post'],'/add', 'System\RoleController@add');     //角色添加
                Route::match(['get', 'post'],'/update', 'System\RoleController@update');  //角色编辑
                Route::match(['get', 'post'],'/menu', 'System\RoleController@menu');  //角色赋予权限

            });

            Route::prefix('admin')->group(function (){
                //管理员
                Route::get('/index', 'System\ManagerController@index');             //管理员列表
                Route::match(['get', 'post'],'/add', 'System\ManagerController@add');     //添加管理员
                Route::match(['get', 'post'],'/update', 'System\ManagerController@update');  //编辑管理员
                Route::match(['get', 'post'],'/password', 'System\ManagerController@password');  //修改密码

            });

            Route::prefix('operate')->group(function () {
                Route::get('/index', 'System\OperateController@index');  //操作页面
            });

            Route::prefix('config')->group(function (){
                //配置
                Route::get('/index', 'System\ConfigController@index');             //配置列表
                Route::match(['get', 'post'],'/add', 'System\ConfigController@add');     //配置添加
                Route::match(['get', 'post'],'/update', 'System\ConfigController@update');  //配置编辑

            });

        });

    });

    Route::get('/system/menu/icon', 'System\MenuController@icon');    // 图标
});

