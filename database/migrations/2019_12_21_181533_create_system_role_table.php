<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50)->default('')->comment('角色名称');
            $table->string('intro',191)->default('')->comment('角色简介');
            $table->text('auth')->comment('角色权限，以json格式保存id');
            $table->tinyInteger('status')->default(1)->comment('状态1可用，2不可用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_role');
    }
}
