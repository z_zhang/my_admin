<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('pid')->comment('父级id');
            $table->string('module',50)->default('')->comment('模块名');
            $table->string('url',191)->default('')->comment('链接地址(就是规定好的路由)');
            $table->string('title',20)->default('')->comment('菜单标题');
            $table->string('icon',191)->nullable()->comment('菜单图标');
            $table->tinyInteger('sort')->default(1)->comment('排序,按数字从小到大排序');
            $table->tinyInteger('system')->default(1)->comment('是否为系统菜单，系统菜单不可删除 , 1.是 ，2.否');
            $table->tinyInteger('nav')->default(1)->comment('是否为菜单显示，1显示 2不显示');
            $table->tinyInteger('status')->default(1)->comment('状态1启用，2禁用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_menu');
    }
}
