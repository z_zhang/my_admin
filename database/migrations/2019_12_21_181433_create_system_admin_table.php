<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_admin', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->unsignedInteger('role_id')->default(0)->comment('角色id');
            $table->char('username',50)->unique()->comment('账号');
            $table->string('password',191)->default('');
            $table->string('nick',50)->default('')->comment('昵称');
            $table->char('mobile',15)->unique();
            $table->char('email',50)->unique();
            $table->string('last_login_ip',128)->default('')->comment('最近一次登录的ip');
            $table->timestamp('last_login_time')->nullable()->comment('最近一次登录的时间');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_admin');
    }
}
