<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_config', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('name',20)->unique()->default('')->comment('唯一主键');
            $table->string('title',50)->default('')->comment('标题');
            $table->string('group',50)->default('')->comment('分组');
            $table->tinyInteger('type')->default(1)->comment('配置类型，1.文本，2.数组，3.键值对，4.图片,5.开关');
            $table->tinyInteger('status')->default(1)->comment('状态1可用，2不可用');

            $table->text('value')->comment('值');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_system');
    }
}
