<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_operate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('admin_id')->default(0)->comment('用户id');
            $table->unsignedInteger('menu_id')->default(0)->comment('菜单id');
            $table->string('method',50)->default('')->comment('http请求method');
            $table->text('params')->comment('求参');
            $table->timestamp('operate_time')->nullable()->comment('操作时间');
            $table->timestamp('created_at')->nullable()->comment('创建时间');
            $table->index('admin_id');
            $table->index('menu_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_operate');
    }
}
