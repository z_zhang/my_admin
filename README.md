# my_admin

#### 介绍
本套系统是laravel6.x和layui2.x结合开发的。开发这套后台系统是我从事开发几年以来比较想做的事情，在2020年春节期间终于有大量的时间把它给实现了。
这套后台系统主要借鉴于hisiphp后台系统( https://www.hisiphp.com. 它的pro版本更加强大哦！)，它主要是thinkphp和layui结合的产物，而我最近两三年都是用laravel开发，因此我特别想在它的一些基础上融入laravel和我自己的一些想法。借着这假期我终于实现了它.
 当然，目前只是发布了最基础的版本，我还没添加很多功能，罗马也不是一日建成的，对吧？后期我会慢慢融入一些想法。
 也欢迎广大朋友下载拉取，要是喜欢这套系统，记得点个赞。要是有什么想法的话可以提issue，我一定会认真考虑一下你的建议。
 qq群交流：1042059806   

#### 软件架构
0. php版本 : 7.2.21
1. laravel: 6.x
2. layui: 2.x
3. mysql: 5.6 版本以上， 建议8.0 效率更高
4. linux: 4.15.0-29deepin-generic (用centos7.x 都ok,windows和mac没试过)
5. nginx: 1.14.2
6. 建议：
    安装redis,并且装上phpredis

#### 安装教程
0.  假设朋友能懂laravel和layui，知道自己如何安装然后搞虚拟域名，然后往下阅读
1.  git clone 下来之后，在项目目录下 composer install
2.  cp .env.example  .env
    对.env写入几个关键配置
    APP_ADMIN_DOMAIN 和 APP_URL : 后台域名
    APP_DOMAIN : 前台域名 (目前未开发，不重要)
    写上自己的数据库配置
    
3.  执行 php artisan key:generate
4.  执行 php artisan storage:link
5.  执行 php artisan migrate 和 php artisan db:seed
6.  默认的后台管理账号和密码是 admin，123456，进入后台界面之后可以在右上角"个人资料"里面修改自己的密码，切记，这个比较重要


#### 一些使用说明

1 本套系统在.env的APP_DEBUG打开的情况会自动记录每个页面执行的sql都会记录到laravel.log日志中，方便开发者查看自己的sql语句，而且还有执行时间
  此外，除了laravel自带的日志之外，还加了基于Monolog二次封装的日志， 使用比较简单： LoggerFacade::info('niu','记录日志',[]),这样在 storage/logs/20200130/niu-2020-01-30.log记录你写的 记录日志。 其中20200130和niu-2020-01-30只是打个比方，是记录当天日期的.注意，要引入use App\Common\Facades\LoggerFacade哦.可以在app/Common看看

2 本套系统是以模块来进行开发的，比如后台中系统模块，就在Controllers目录下建立admin/System, 如果你想开发一个博客管理的话，你可以在admin下面建立一个Blog目录放博客的控制器.那么在其他的如路由，Service(App/Service目录)，Model目录，Http/Validates目录也是遵循模块思路去做.也是笔者建议去做。模块化(业务模块化)开发适合大部分系统，特别复杂系统例外

3 本套系统可以借鉴Controllers/Admin/System/RoleController和ManagerController两控制器开发。需要说明的是App/Service目录下还分成了Command和Query，Command目录代表的是执行写功能，Query代表的是读功能。

4  安装了laravel-ide-helper
使用说明：

#### 、在指定位置创建model文件，指定表名
    
        protected $table = '表名';
        
#### 、指定model，执行注解生成命令

       php artisan ide-helper:models -W --dir='app/Models'
       
5. 后台操作日志记录事件
       在校验后台权限中间件AdminAuth中，后面有加一个操作日志记录事件: event(new AdminOperateEvent($request));            
       由于该监听器继承了队列机制，因此推荐用队列执行该事件，推荐用redis. 执行 php artisan queue:work  --queue=admin_operate
       结合Supervisor去管理队列更好. 当然，不习惯用队列的话可以用sync驱动，只要在.env里面设置QUEUE_CONNECTION=sync 就行了 

