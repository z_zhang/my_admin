<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Validates\Admin\LoginValidates;
use App\Service\Command\Admin\LoginCommandService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    protected $service = LoginCommandService::class;

    public function login(Request $request)
    {
        if ( $request->ajax() ) {
            $request->validate(LoginValidates::$rule,[
                'captcha.required' => '验证码不能为空',
                'captcha.captcha' => '请输入正确的验证码',]);

            return app($this->service)->login($request->all());
        }

        return view('admin.login');
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        return redirect('/login');
    }



}