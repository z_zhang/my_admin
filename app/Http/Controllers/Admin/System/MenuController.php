<?php


namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Validates\Admin\System\MenuValidates;
use App\Models\System\SystemMenuModel;
use App\Service\Command\Admin\System\MenuCommandService;
use App\Service\Query\Admin\System\MenuQueryService;
use Illuminate\Http\Request;

class MenuController extends BaseController
{
    protected $queryService   = MenuQueryService::class;
    protected $commandService = MenuCommandService::class;
    protected $model= SystemMenuModel::class;
    protected $validate= MenuValidates::class;
    protected $viewAuthRoute = [ 'add'=>'/system/menu/add','update'=>'/system/menu/update' ];

    //展示个人信息
    public function index()
    {
        $lists = app($this->queryService)->index();
        $viewAuthBool = $this->checkViewAuth($this->viewAuthRoute);

        return view('admin.system.menu.index',['lists'=>$lists,'viewAuthBool'=>$viewAuthBool]);
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            return parent::add($request);
        }

        $queryService = app($this->queryService);

        $menusOptions = $queryService->getMenuOption($queryService->index(),$request->input('pid',0),0);


        return view('admin.system.menu.form',['menusOptions'=>$menusOptions]);
    }

    public function update(Request $request)
    {

        if ($request->ajax()) {
            return parent::update($request);
        }

        $this->model = app($this->model);
        $info = $this->model->find($request->input('id'));

        if (empty($info)) {
            abort(404);
        }

        $queryService = app($this->queryService);

        $menusOptions = $queryService->getMenuOption($queryService->index(),$info->pid,0);

        return view('admin.system.menu.form',['menusOptions'=>$menusOptions,'info'=>$info]);
    }

    //图标
    public function icon()
    {
        return view('admin.system.menu.icon');
    }

}