<?php


namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\BaseController;
use App\Models\System\SystemAdminModel;
use App\Models\System\SystemMenuModel;
use App\Models\System\SystemOperateModel;
use App\Service\Query\Admin\System\MenuQueryService;
use App\Service\Query\Admin\System\OperateQueryService;
use Illuminate\Http\Request;

class OperateController extends BaseController
{
    protected $queryService   = OperateQueryService::class;
    protected $model= SystemOperateModel::class;

    //列表信息
    public function index(Request $request)
    {
        if ( $request->ajax() ) {
            return $this->lists_index();
        }

        $admins =  SystemAdminModel::all(['id','nick'])->toArray();
        $MenuQueryService = new MenuQueryService(new SystemMenuModel());
        $menusOptions = $MenuQueryService->getMenuOption($MenuQueryService->index(),0,0);

        return view('admin.system.operate.index',['menus'=>$menusOptions,'admins'=>$admins]);
    }



}