<?php


namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Validates\Admin\System\RoleValidates;
use App\Models\System\SystemRoleModel;
use App\Service\Command\Admin\System\RoleCommandService;
use App\Service\Query\Admin\System\MenuQueryService;
use App\Service\Query\Admin\System\MyInfoQueryService;
use App\Service\Query\Admin\System\RoleQueryService;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    protected $queryService   = RoleQueryService::class;
    protected $commandService = RoleCommandService::class;
    protected $model= SystemRoleModel::class;
    protected $validate= RoleValidates::class;
    protected $viewAuthRoute = ['add'=>'/system/role/add','update'=>'/system/role/update','menu'=>'/system/role/menu'];

    //列表信息
    public function index()
    {
        return $this->lists_index();
    }

    //对角色赋予权限
    public function menu(Request $request)
    {

        if ($request->ajax()) {
            $validate = app($this->validate);
            $request->validate( $validate->roleRule );

            return app($this->commandService)->menu($request->all());

        }

        $info = app($this->queryService)->info($request->input('id'));
        $menuQueryService = app(MenuQueryService::class);
        $menuTree = $menuQueryService->index();

        $menuQueryService->getMenuCheck($menuTree,json_decode($info->auth,true));

        return view('admin.system.role.menu',['info'=>$info,'menus'=>$menuTree]);
    }

}