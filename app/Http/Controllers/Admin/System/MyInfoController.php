<?php


namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Validates\Admin\System\MyInfoValidates;
use App\Service\Command\Admin\System\MyInfoCommandService;
use App\Service\Query\Admin\System\MyInfoQueryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class MyInfoController extends BaseController
{
    protected $queryService   = MyInfoQueryService::class;
    protected $commandService = MyInfoCommandService::class;
    protected $validate = MyInfoValidates::class;
    protected $viewAuthRoute = [ 'update'=>'/my/update','password'=>'/my/password' ];


    //展示个人信息
    public function index()
    {

        $info = app($this->queryService)->info();
        $viewAuthBool = $this->checkViewAuth($this->viewAuthRoute);

        return view('admin.system.my-info.index',['user'=>$info,'viewAuthBool'=>$viewAuthBool]);
    }


    public function update(Request $request)
    {

        if ( $request->ajax() ) {

            $request->validate( app($this->validate)->updateRule );

            return app($this->commandService)->update($request->all()) ;
        }

        $info = app($this->queryService)->info();

        return view('admin.system.my-info.form',['info'=>$info]);
    }

    public function password(Request $request)
    {
        if ( $request->ajax()  ) {

            $request->validate( app($this->validate)->password );
            return app($this->commandService)->password($request->all()) ;
        }

        return view('admin.system.my-info.password');
    }

}