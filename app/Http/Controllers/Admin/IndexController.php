<?php


namespace App\Http\Controllers\Admin;

use App\Models\System\SystemConfigModel;
use App\Service\Query\Admin\System\MenuQueryService;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    protected $queryService = MenuQueryService::class;
    protected $viewAuthRoute = [ 'info'=>'/my/index' ];

    public function index(Request $request)
    {
        if ( $request->has('other') ) {


            return '<div>首页信息</div>';
        }

        $threeMenus = session('threeMenus');

        if ( empty($threeMenus) ) {
            $threeMenus = app($this->queryService)->getThreeMenu();
            session(['threeMenus'=>$threeMenus]);
        }

        $viewAuthBool = [ 'viewAuthBool' => $this->checkViewAuth($this->viewAuthRoute)];
        $backstageTitle = SystemConfigModel::getOne('backstage_title')??'个人后台';
        $logo = SystemConfigModel::getOne('logo')??'';
        $result = array_merge($threeMenus,$viewAuthBool,['backstageTitle' => $backstageTitle,'logo'=>$logo]);

        return view('admin.index',$result);
    }


}