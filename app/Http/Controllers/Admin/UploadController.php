<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Response\JsonResult;
use App\Http\Response\ResponseCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class UploadController extends Controller
{

    public function index(Request $request)
    {
        if ( Config::get('app.my_upload')  ) {
            return $this->myUpload($request);
        } else {
            return $this->upload($request);
        }
    }

    //本套系统实现的上传文件逻辑，不是oss，要想实现oss的逻辑，开发者可以在myUpload方法里实现自己的逻辑
    protected function upload(Request $request)
    {
        if ( $request->hasFile('file') && $request->file('file')->isValid() ) {
            $file = $request->file('file');
            $dir = '/upload/'.date('Ymd');
            $extension = $file->extension();
            $src = $file->store($dir);

            if ( $src ) {
                $src = '/'.$src;
                return JsonResult::returnJson(ResponseCode::SUCCESS,['src'=>$src,'img_src'=>Config::get('filesystems.disks.public.url'). $src ]);
            } else {
                return JsonResult::returnJson(ResponseCode::UPLOAD_FAIL);
            }
        }

        return JsonResult::returnJson(ResponseCode::UPLOAD_FAIL,[],'文件格式不正确');
    }

    //这里是实现开发者自己的上传文件逻辑
    protected function myUpload(Request $request)
    {

    }

}