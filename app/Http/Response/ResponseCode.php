<?php


namespace App\Http\Response;


class ResponseCode
{
    const  SUCCESS ='00000';

    //用户相关
    const  USER_NOT_EXIST  = '10001';  //用户不存在
    const  USER_PASS_ERROR = '10002';  //密码错误
    const  USER_EXIST_OR_FAIL_PASS = '10003';  //账号或者密码错误
    const  LOGIN_CODE_ERROR = '10004';  //验证码错误
    const  USER_ALREADY_EXISTS= '10005';  //账号已经重复
    const  MOBILE_ALREADY_EXISTS= '10006';  //手机号已经重复
    const  EMAIL_ALREADY_EXISTS= '10007';  //邮箱已经重复
    const  OLD_PASS_ERROR= '10008';         //旧密码错误

    //数据描述
    const  DATA_NOT_EXIST  = '11001';  //数据不存在
    const  DATA_ALREADY_EXIST  = '11002';  //数据重复了
    const  KEY_ALREADY_EXIST  = '11003';  //key重复了


    //system模块
    const  MENU_URL_EXIST  = '12001';  // 路由地址重复了


    //系统错误
    const   SYSTEM_ERROR_METHOD  = '00001';  //请求方法不正确
    const   EXCEPTION_MESSAGE  = '00002';  //异常信息，可以自定义信息
    const   MODEL_NO_DEFINE  = '00003';  //模型没有在控制器上定义
    const   AUTH_NO_PASS  = '00004';     //没有通过权限
    const   UPLOAD_FAIL  = '00005';     //文件上传失败
    const   VALIDATE_FAIL  = '00006';     //校验数据未通过


}