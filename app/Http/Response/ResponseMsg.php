<?php


namespace App\Http\Response;



class ResponseMsg
{
    public static function getCodeMsg($code)
    {
        return self::$msg[$code];
    }


    protected static $msg= [
        ResponseCode::SUCCESS => '成功',

        //用户相关
        ResponseCode::USER_NOT_EXIST => '用户不存在',
        ResponseCode::USER_PASS_ERROR => '密码错误',
        ResponseCode::USER_EXIST_OR_FAIL_PASS => '账号或者密码错误',
        ResponseCode::LOGIN_CODE_ERROR => '验证码错误',
        ResponseCode::USER_ALREADY_EXISTS => '账号已经重复',
        ResponseCode::MOBILE_ALREADY_EXISTS => '手机号已经重复',
        ResponseCode::EMAIL_ALREADY_EXISTS => '邮箱已经重复',
        ResponseCode::OLD_PASS_ERROR => '旧密码错误',

        //数据描述
        ResponseCode::DATA_NOT_EXIST => '数据不存在',
        ResponseCode::DATA_ALREADY_EXIST => '数据重复了',
        ResponseCode::KEY_ALREADY_EXIST => 'key重复了',

        //system模块
        ResponseCode::MENU_URL_EXIST => '路由地址重复了',

        //系统错误
        ResponseCode::SYSTEM_ERROR_METHOD => '请求方法不正确',
        ResponseCode::EXCEPTION_MESSAGE => '异常',
        ResponseCode::MODEL_NO_DEFINE =>   '模型没有在控制器上定义',
        ResponseCode::AUTH_NO_PASS =>   '没有通过权限',
        ResponseCode::UPLOAD_FAIL =>   '文件上传失败',
        ResponseCode::VALIDATE_FAIL =>   '校验数据未通过',

    ];


}