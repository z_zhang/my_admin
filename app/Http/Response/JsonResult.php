<?php


namespace App\Http\Response;


class JsonResult
{
    public static function returnJson($code,$data=[],$msg='')
    {
      if ( empty($msg) ) {
          $msg = ResponseMsg::getCodeMsg($code);
      }

      return  response()->json([
                'code' => $code,
                'msg'  => $msg,
                'data' => $data,
      ]);
    }


}