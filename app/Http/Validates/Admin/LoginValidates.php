<?php


namespace App\Http\Validates\Admin;


class LoginValidates
{
    static $rule = [
        'username' => 'required|string|max:50',
        'password' => 'required|string|min:6',
        'captcha' => ['required', 'captcha'],
    ];
}