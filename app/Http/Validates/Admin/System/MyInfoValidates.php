<?php


namespace App\Http\Validates\Admin\System;

class MyInfoValidates
{
    public $updateRule = [
        'nick' =>  ['required','string','max:50'],
        'username' => ['required','string','max:50'],
        'mobile' => ['required','string','max:11','phoneCheck' ],
        'email' => ['required','string','email','max:50'],
    ];

    public $password = [
        'old_password' =>  ['required','string','between:6,12'],
        'password' =>  ['required','string','between:6,12','confirmed'],
    ];


}