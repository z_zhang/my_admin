<?php


namespace App\Http\Validates\Admin\System;

class AdminValidates
{
    public $addRule = [
        'role_ids' =>   ['required'],
        'username'=> ['required','string','max:50','unique:system_admin'],
        'nick' =>   ['required','string','max:50'],
        'mobile' => ['required','string','max:11','phoneCheck','unique:system_admin' ],
        'email' => ['required','string','max:50','email','unique:system_admin'],
    ];

    public $updateRule = [
        'id' => ['gt:1'],
        'role_ids' =>   ['required'],
        'username'=> ['required','string','max:50'],
        'nick' =>   ['required','string','max:50'],
        'mobile' => ['required','string','max:11','phoneCheck'],
        'email' => ['required','string','max:50','email'],
    ];

    public $password = [
        'password' =>  ['required','string','between:6,12'],
    ];


}