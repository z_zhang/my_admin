<?php


namespace App\Http\Validates\Admin\System;

class ConfigValidates
{
    public $addRule = [
        'name' => ['required','string','max:50','unique:system_config'],
        'title' => ['required','string','max:50'],
        'group' => ['string','max:50'],
        'status' => ['in:1,2'],
        'value' => ['required'],
    ];

    public $updateRule = [
        'name' => ['required','string','max:50'],
        'title' => ['required','string','max:50'],
        'group' => ['string','max:50'],
        'status' => ['in:1,2'],
        'value' => ['required'],
    ];

    public $roleRule = [
        'id' => ['gt:1','exists:system_role'],
    ];


}