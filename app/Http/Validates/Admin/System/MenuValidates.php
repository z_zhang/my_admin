<?php


namespace App\Http\Validates\Admin\System;

class MenuValidates
{
    public $addRule = [
        'pid' =>   ['required','integer'],
        'module'=> ['required','string','max:50'],
        'url' =>   ['required','string','max:50','unique:system_menu'],
        'title' =>   ['required','string','max:50'],
        'sort' =>   ['integer'],
        'system' =>   ['in:1,2'],
        'nav' =>   ['in:1,2'],
    ];

    public $updateRule = [
        'id'   => ['exists:system_menu'],
        'pid' =>   ['required','integer'],
        'module'=> ['required','string','max:50'],
        'url' =>   ['required','string','max:50'],
        'title' =>   ['required','string','max:50'],
        'sort' =>   ['integer'],
        'system' =>   ['in:1,2'],
        'nav' =>   ['in:1,2'],
    ];




}