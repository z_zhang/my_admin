<?php


namespace App\Http\Validates\Admin\System;

class RoleValidates
{
    public $addRule = [
        'name' => ['required','string','max:50'],
        'intro' => ['required','string','max:191'],
        'status' => ['in:1,2'],
    ];

    public $updateRule = [
        'id' => ['gt:1'],
        'name' => ['required','string','max:50'],
        'intro' => ['required','string','max:191'],
        'status' => ['in:1,2'],
    ];

    public $roleRule = [
        'id' => ['gt:1','exists:system_role'],
    ];


}