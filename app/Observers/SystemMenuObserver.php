<?php

namespace App\Observers;

//菜单事件
use App\Common\CacheNameManager;
use Illuminate\Support\Facades\Cache;

class SystemMenuObserver
{

    public function saved()
    {
        //删除菜单相关的缓存
        Cache::forget(CacheNameManager::MENU_TREE);
        Cache::forget(CacheNameManager::MENU_URL_ID);
    }

}
