<?php

namespace App\Observers;

// 系统配置事件
use App\Common\CacheNameManager;
use App\Models\System\SystemConfigModel;
use Illuminate\Support\Facades\Cache;

class SystemConfigObserver
{

    public function saved(SystemConfigModel $configModel)
    {
        //删除配置相关的缓存
        Cache::forget( sprintf(CacheNameManager::SYSTEM_CONFIG,$configModel->name) );

    }

}
