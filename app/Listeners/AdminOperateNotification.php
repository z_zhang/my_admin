<?php

namespace App\Listeners;

use App\Events\AdminOperateEvent;
use App\Models\System\SystemOperateModel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AdminOperateNotification implements ShouldQueue
{

    /**
     * 任务将被推送到的连接名称.
     *
     * @var string|null
     */
    public $queue = 'admin_operate';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminOperateEvent  $event
     * @return void
     */
    public function handle(AdminOperateEvent $event)
    {
        //先过滤一下$event的params一些敏感参数
        if ( isset( $event->attributes['params']['password'] ) ) {
            unset($event->attributes['params']['password']);
        }
        if ( isset( $event->attributes['params']['old_password'] ) ) {
            unset($event->attributes['params']['old_password']);
        }

        if ( isset( $event->attributes['params']['password_confirmation'] ) ) {
            unset($event->attributes['params']['password_confirmation']);
        }
//        $event->attributes['params'] = json_encode($event->attributes['params']);
        $event->attributes['created_at'] =  Carbon::now()->toDateTimeString();
        SystemOperateModel::create($event->attributes);

    }
}
