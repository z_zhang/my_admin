<?php
namespace App\Models\System;

use App\Observers\SystemMenuObserver;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\System\SystemMenuModel
 *
 * @property int $id
 * @property int $pid 父级id
 * @property string $module 模块名
 * @property string|null $url 链接地址(就是规定好的路由)
 * @property string $title 菜单标题
 * @property string $icon 菜单图标, 默认aicon ai-shezhi
 * @property int $sort 排序
 * @property int $system 是否为系统菜单，系统菜单不可删除 , 1.是 ，2.否
 * @property int $nav 是否为菜单显示，1显示 2不显示
 * @property int $status 状态1显示，2隐藏
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereNav($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemMenuModel whereUrl($value)
 * @mixin \Eloquent
 */
class SystemMenuModel extends Model
{
    protected $table='system_menu';

    protected $fillable = ['pid','module','url','title','icon','sort','system','nav'];


    protected static function boot()
    {
        parent::boot();
        static::observe(new SystemMenuObserver());  //注册观察者
    }

}