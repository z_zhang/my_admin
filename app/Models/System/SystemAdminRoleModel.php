<?php
namespace App\Models\System;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


/**
 * App\Models\System\SystemAdminRoleModel
 *
 * @property int $id
 * @property int $admin_id 管理员id
 * @property int $role_id 角色id
 * @property string|null $created_at 创建时间
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\System\SystemRoleModel $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminRoleModel whereRoleId($value)
 * @mixin \Eloquent
 */
class SystemAdminRoleModel extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['admin_id','role_id'];

    protected $table = 'system_admin_role';
    public $timestamps=false;

    public $auth = [];

}