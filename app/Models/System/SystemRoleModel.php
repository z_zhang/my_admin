<?php
namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\System\SystemRoleModel
 *
 * @property int $id
 * @property string $name 角色名称
 * @property string $intro 角色简介
 * @property string $auth 角色权限，以json格式保存id
 * @property int $status 状态1可用，2不可用
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereAuth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemRoleModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SystemRoleModel extends Model
{
    protected $table='system_role';
    protected $fillable = ['name','intro','status','auth'];

    const SUPER_ADMIN = 1; //超级管理员

    const STATUS=[
        'USE'=>1,      //可用
        'DISABLE'=>2,  //不可用
    ];

    const STATUS_TEXT = [
        1 => '可用',
        2 => '不可用',
    ];


}