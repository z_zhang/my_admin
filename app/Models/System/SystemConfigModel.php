<?php
namespace App\Models\System;

use App\Common\CacheNameManager;
use App\Observers\SystemConfigObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


/**
 * App\Models\System\SystemConfigModel
 *
 * @property int $id
 * @property string $name 唯一主键
 * @property string $title 标题
 * @property string $group 分组
 * @property int $type 配置类型，1.文本，2.数组，3.键值对，4.图片, 5.开关
 * @property int $status 状态1可用，2不可用
 * @property string $value 值
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemConfigModel whereValue($value)
 * @mixin \Eloquent
 */
class SystemConfigModel extends Model
{
    protected $table='system_config';
    protected $fillable = ['name','title','group','type','value','status'];

    const TYPE = [
        'text' => 1,
        'array' => 2,
        'map' => 3,
        'img' => 4,
        'switch' => 5,
    ];

    const TYPE_TEXT = [
        1 => '文本',
        2 => '数组',
        3 => '哈希',
        4 => '图像',
        5 => '开关',
    ];

    const STATUS=[
        'USE'=>1,      //可用
        'DISABLE'=>2,  //不可用
    ];

    const STATUS_TEXT = [
        1 => '可用',
        2 => '不可用',
    ];


    protected static function boot()
    {
        parent::boot();
        static::observe(new SystemConfigObserver());  //注册观察者
    }

    //获取单个配置的缓存
    public static function getOne($key)
    {
        $cacheValue =  Cache::get(sprintf(CacheNameManager::SYSTEM_CONFIG,$key) );
        if (empty($cacheValue)) {
            $systemConfig = SystemConfigModel::whereName($key)->whereStatus(SystemConfigModel::STATUS['USE'])->first();

            if ( empty($systemConfig) ) {
               $cacheValue = '';

            } elseif ( SystemConfigModel::TYPE['img']  == $systemConfig->type ) {
                if ( empty($systemConfig->value) ) {
                    $cacheValue = '';
                } else {
                    $cacheValue =  config('filesystems.disks.public.url').$systemConfig->value;
                }

            } elseif ( SystemConfigModel::TYPE['array']  == $systemConfig->type ) {
                $cacheValue = json_decode($systemConfig->value,true) ;

            } elseif ( SystemConfigModel::TYPE['map']  == $systemConfig->type ) {
                $cacheValue = json_decode($systemConfig->value,true) ;

            } else {
                $cacheValue = $systemConfig->value;
            }

            Cache::put(sprintf(CacheNameManager::SYSTEM_CONFIG,$key),$cacheValue);

        }

        return $cacheValue;
    }


}