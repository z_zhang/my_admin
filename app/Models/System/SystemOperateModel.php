<?php
namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\System\SystemOperateModel
 *
 * @property int $id
 * @property int $admin_id 用户id
 * @property int $menu_id 菜单id
 * @property string $method http请求method
 * @property string $params 求参
 * @property string|null $operate_time 操作时间
 * @property string|null $created_at 创建时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereOperateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemOperateModel whereParams($value)
 * @mixin \Eloquent
 */
class SystemOperateModel extends Model
{
    public $timestamps=false;
    protected $fillable = ['admin_id','menu_id','method','params','operate_time','created_at'];

    protected $table = 'system_operate';

    protected $casts = [
        'params' => 'array',
    ];

    public function getParamsAttribute($value)
    {
        if ( $value == '[]' ) {
            return '';
        }

        return  json_encode(json_decode($value,true) ,JSON_UNESCAPED_UNICODE)  ;
    }
}