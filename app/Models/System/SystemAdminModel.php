<?php
namespace App\Models\System;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\System\SystemAdminModel
 *
 * @property int $id
 * @property int $role_id 角色id
 * @property string $username 账号
 * @property string $password
 * @property string $nick 昵称
 * @property string $mobile
 * @property string $email
 * @property string $last_login_ip 最近一次登录的ip
 * @property string|null $last_login_time 最近一次登录的时间
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereLastLoginTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereNick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\SystemAdminModel whereUsername($value)
 * @mixin \Eloquent
 * @property-read \App\Models\System\SystemRoleModel $role
 */
class SystemAdminModel extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['role_id','username','nick','mobile','email','system','nav'];

    protected $table = 'system_admin';

    public $auth = [];

//    public function role()
//    {
//        return $this->hasOne(SystemRoleModel::class,'id','role_id');
//    }

    public function roles()
    {
        return $this->belongsToMany(SystemRoleModel::class,'system_admin_role','admin_id','role_id');
    }

}