<?php
/**
 * Created by PhpStorm.
 * User: laozhou
 * Date: 2020-03-08
 * Time: 10:02
 */

namespace App\Exceptions;


class ApiException  extends \RuntimeException
{
    protected $data = [];
    protected $apiCode = '';

    public function __construct( $code = '',$data=[],  $message = "") {
       $this->apiCode = $code;
       $this->message = $message;
       $this->data = $data;
       parent::__construct($message,200,null);

    }

    public function getData() {
        return $this->data;
    }

    public function getApiCode() {
        return $this->apiCode;
    }



}