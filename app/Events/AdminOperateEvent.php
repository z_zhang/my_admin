<?php

namespace App\Events;

use App\Common\CheckMenu;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class AdminOperateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $attributes;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->attributes['admin_id'] = session('admin')['id'];
        $menu_id =  CheckMenu::getUrlToId('/'.$request->path());
        if ( empty($menu_id) ) {
            $menu_id =  CheckMenu::getUrlToId($request->path());
        }
        $this->attributes['menu_id'] = $menu_id;
        $this->attributes['method'] = $request->getMethod();
        if ( $request->ajax() ) {
            $this->attributes['method'] .= '_ajax';
        }

        $this->attributes['params'] = $request->all();
        $this->attributes['operate_time'] = Carbon::now()->toDateTimeString();

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
