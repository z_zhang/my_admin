<?php

namespace App\Providers;

use App\Common\Log\MyLogger;
use App\Models\System\SystemConfigModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Logger', function () {
            return new  MyLogger();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $backstageTitle = SystemConfigModel::getOne('backstage_title')??'个人后台';
//        View::share('backstageTitle',$backstageTitle);

        Validator::extend('phoneCheck', function($attribute, $value, $parameters)
        {
            if(preg_match('/^1[3-9][0-9]{9}$/',$value)==0){
                return false;
            }
            return true;
        });
    }
}
