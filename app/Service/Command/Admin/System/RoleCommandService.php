<?php
namespace App\Service\Command\Admin\System;

use App\Http\Response\JsonResult;
use App\Http\Response\ResponseCode;
use App\Models\System\SystemMenuModel;
use App\Models\System\SystemRoleModel;

class RoleCommandService
{
    //赋予权限
    public function menu($params)
    {
        $role = SystemRoleModel::find($params['id']);

        $auth = json_encode(array_values($params['menuids']));
        $role->auth = $auth;

        $role->save();

        return JsonResult::returnJson(ResponseCode::SUCCESS);
    }


}