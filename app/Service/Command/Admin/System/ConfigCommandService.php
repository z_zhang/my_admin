<?php
namespace App\Service\Command\Admin\System;

use App\Http\Response\JsonResult;
use App\Http\Response\ResponseCode;
use App\Models\System\SystemConfigModel;

class ConfigCommandService
{
    public function update(SystemConfigModel $configModel, $params)
    {

        $validateResult =  $this->updateValidate($configModel,$params);

        if ( true !== $validateResult ) {
            return $validateResult;
        }

        foreach ($params as $key => $param) {
            if ( isset($configModel->$key) ) {
                $configModel->$key = $param;
            }
        }

        $configModel->save();

        return JsonResult::returnJson(ResponseCode::SUCCESS);
    }

    /**
     * 更新校验
     * @param $config
     * @param $params
     * @return bool|\Illuminate\Http\JsonResponse
     */
    private function updateValidate($config, $params)
    {
        if ( $config->name != $params['name'] ) {
              if ( SystemConfigModel::where('name',$params['name'])->exists() ) {
                  return JsonResult::returnJson(ResponseCode::KEY_ALREADY_EXIST);
              }

        }

        return true;
    }




}