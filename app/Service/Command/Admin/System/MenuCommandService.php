<?php
namespace App\Service\Command\Admin\System;

use App\Http\Response\JsonResult;
use App\Http\Response\ResponseCode;
use App\Models\System\SystemMenuModel;

class MenuCommandService
{

    public function update(SystemMenuModel $menuModel,$params)
    {

        if ( $params['url'] != $menuModel->url ) {

            if ( SystemMenuModel::whereUrl($params['url'])->exists() ) {
                return JsonResult::returnJson(ResponseCode::MENU_URL_EXIST);
            }
        }

        foreach ($params as $key => $param) {
            if ( isset($menuModel->$key) ) {
                $menuModel->$key = $param;
            }
        }

        $menuModel->save();

        return JsonResult::returnJson(ResponseCode::SUCCESS);
    }

}