<?php
namespace App\Service\Command\Admin\System;

use App\Http\Response\JsonResult;
use App\Http\Response\ResponseCode;
use App\Models\System\SystemAdminModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MyInfoCommandService
{
    public function update($params)
    {
        $user = Auth::guard('admin')->user();

        $validateResult =  $this->updateValidate($user,$params);

        if ( true !== $validateResult ) {
            return $validateResult;
        }

        foreach ($params as $key => $param) {
            if ( isset($user->$key) ) {
                $user->$key = $param;
            }
        }

        $user->save();
        Auth::guard('admin')->setUser($user);
        session(['admin'=>$user]);

        return JsonResult::returnJson(ResponseCode::SUCCESS);
    }

    /**
     * 更新校验
     * @param $user
     * @param $params
     * @return bool|\Illuminate\Http\JsonResponse
     */
    private function updateValidate($user,$params)
    {
        if ( $user->username != $params['username'] ) {
              if ( SystemAdminModel::whereUsername($params['username'])->exists() ) {
                  return JsonResult::returnJson(ResponseCode::USER_EXIST_OR_FAIL_PASS);
              }
        }

        if ( $user->mobile != $params['mobile'] ) {
            if ( SystemAdminModel::whereMobile($params['mobile'])->exists() ) {
                return JsonResult::returnJson(ResponseCode::MOBILE_ALREADY_EXISTS);
            }
        }

        if ( $user->email != $params['email'] ) {
            if ( SystemAdminModel::whereEmail($params['email'])->exists() ) {
                return JsonResult::returnJson(ResponseCode::EMAIL_ALREADY_EXISTS);
            }
        }

        return true;
    }


    public function password($params)
    {
        $user = Auth::guard('admin')->user();

        if (  !Hash::check($params['old_password'],$user->password)  ) {
            return JsonResult::returnJson(ResponseCode::OLD_PASS_ERROR);
        }

        $user->password = bcrypt($params['password']);

        $user->save();
        Auth::guard('admin')->setUser($user);

        return JsonResult::returnJson(ResponseCode::SUCCESS);
    }

}