<?php


namespace App\Service\Query\Admin\System;

use App\Models\System\SystemConfigModel;
use App\Service\Query\Common\CommonQuery;
use Illuminate\Support\Facades\Config;

class ConfigQueryService
{
    use CommonQuery;

    public function __construct(SystemConfigModel $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $result = $this->getListsAndCount();
        $lists = $result['lists'];

        foreach ($lists as &$list) {
            $list['type_text']   =  SystemConfigModel::TYPE_TEXT[$list['type']];
            $list['status_text'] =  SystemConfigModel::STATUS_TEXT[$list['status']];

            if ( SystemConfigModel::TYPE['img'] == $list['type'] ) {
                $list['img_src'] = Config::get('filesystems.disks.public.url'). $list['value'] ;
            }

        }


        return $result;
    }

}