<?php


namespace App\Service\Query\Admin\System;


use App\Models\System\SystemAdminModel;
use App\Models\System\SystemMenuModel;
use App\Models\System\SystemOperateModel;
use App\Models\System\SystemRoleModel;
use App\Service\Query\Common\CommonQuery;

class OperateQueryService
{
    use CommonQuery;

    public function __construct(SystemOperateModel $model)
    {
        $this->model = $model;
    }

    public function index()
    {

        $this->searchWhere();

        $result = $this->getListsAndCount();
        $lists = $result['lists'];

        $adminIds = array_unique($lists->pluck('admin_id')->toArray()) ;
        $menuIds =  array_unique($lists->pluck('menu_id')->toArray());

        $admins = SystemAdminModel::whereIn('id',$adminIds)->get(['id','nick']);
        $menus  = SystemMenuModel::whereIn('id',$menuIds)->get(['id','url','title']);
        $admins = $admins->keyBy('id')->toArray();
        $menus   = $menus->keyBy('id')->toArray();

        foreach ($lists as &$list) {
            $list['nick'] = $admins[$list['admin_id']]['nick']??'';
            $list['url'] = $menus[ $list['menu_id'] ]['url']??'';
            $list['title'] = $menus[ $list['menu_id'] ]['title']??'';
        }


        return $result;
    }

    //搜索条件
    protected function searchWhere()
    {
        $params = request()->all();

        $eqWhere = [];
        if ( !empty($params['menu_id']) ) {
            $eqWhere[] = ['menu_id','=',$params['menu_id']];
        }

        if ( !empty($params['admin_id']) ) {
            $eqWhere[] = ['admin_id','=',$params['admin_id']];
        }

        if ( !empty($eqWhere) ) {
            $this->setWheres($eqWhere);
        }
    }

}