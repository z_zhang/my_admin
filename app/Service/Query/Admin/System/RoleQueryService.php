<?php


namespace App\Service\Query\Admin\System;


use App\Models\System\SystemRoleModel;
use App\Service\Query\Common\CommonQuery;
use Illuminate\Support\Facades\DB;

class RoleQueryService
{
    use CommonQuery;

    public function __construct(SystemRoleModel $model)
    {
        $this->model = $model;
    }

    //列表页
    public function index($params=[])
    {
        $result = $this->getListsAndCount();

        foreach ($result['lists'] as &$list) {
            $list['status_text'] = SystemRoleModel::STATUS_TEXT[$list->status];
        }

        return $result;
    }

    public function info($id)
    {
        return $this->model->find($id);
    }


    //获取以id为主的键值对
    public function getIdToData($where)
    {
        $this->model = $this->model->where($where);

        return $this->pluckToArray('name','id');
    }



}