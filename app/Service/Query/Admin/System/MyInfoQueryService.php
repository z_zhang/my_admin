<?php


namespace App\Service\Query\Admin\System;


use Illuminate\Support\Facades\Auth;

class MyInfoQueryService
{


    public function info()
    {
       return Auth::guard('admin')->user();
    }

}