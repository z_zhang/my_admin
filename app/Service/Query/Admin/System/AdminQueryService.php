<?php


namespace App\Service\Query\Admin\System;


use App\Models\System\SystemAdminModel;
use App\Models\System\SystemRoleModel;
use App\Service\Query\Common\CommonQuery;

class AdminQueryService
{
    use CommonQuery;

    public function __construct(SystemAdminModel $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $result = $this->getListsAndCount();
        $lists = $result['lists'];

        $lists->load(['roles' => function ($query) {
            $query->select('name');
        }]);

        return $result;
    }

}