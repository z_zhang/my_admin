<?php
/**
 * Created by PhpStorm.
 * User: niebangheng
 * Date: 2018/9/21
 * Time: 16:16
 */

namespace App\Common\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static  info($type = '', $message='', array $context = [])
 * @method static  error($type = '',  $message='', array $context = [])
 *
 * @see App\Common\Log\MyLogger
 */
class LoggerFacade extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'Logger';
    }
}