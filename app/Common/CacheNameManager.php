<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 20-1-11
 * Time: 下午4:42
 */

namespace App\Common;

//缓存名称管理
class CacheNameManager
{

    //系统模块
    const MENU_TREE = 'menu_tree';   //菜单树型
    const MENU_URL_ID = 'menu_url_id';   //菜单 url对应id数组结构 ， [ '/index'=>1 ]
    const SYSTEM_CONFIG = 'system_config_%s';   //系统配置

}