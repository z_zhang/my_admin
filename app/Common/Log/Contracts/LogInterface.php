<?php
/**
 * Created by PhpStorm.
 * User: develoop
 * Date: 19-2-16
 * Time: 上午11:28
 */

namespace App\Common\Log\Contracts;

interface LogInterface
{
    /**
     * @param $message,日志记录消息体
     * @param string $type, 文件名
     * @param array $context,数组内容
     * @return mixed
     */
    public function info($message,$type = '', array $context = []);

}