<?php
/**
 * Created by PhpStorm.
 * User: develoop
 * Date: 19-2-16
 * Time: 上午9:16
 */

namespace App\Common\Log;

use App\Common\Log\Contracts\LogInterface;
use Illuminate\Support\Facades\Config;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FormattableHandlerInterface;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class MyLogger  implements LogInterface
{
    protected $instances=[];

    public function useDailyFiles($method){
        $dirpath = storage_path().'/logs/';


        if ( !isset($this->instances[$method]) ) {
            $config = [
                'path' => $dirpath.date('Y-m-d').'/'.$method.'.log',
                ''
            ];

            $name = Config::get('app.env','production');

            $this->instances[$method] = new Logger($name, [
                $this->prepareHandler(new RotatingFileHandler(
                    $config['path'], $config['days'] ?? 7, 'debug',
                    $config['bubble'] ?? true, $config['permission'] ?? null, $config['locking'] ?? false
                ), $config),
            ]);
        }

    }

    public function error( $type = '',$message='', array $context = [])
    {
        $type = empty($type) ? __FUNCTION__ : $type;
        $this->useDailyFiles($type);

        $this->instances[$type]->error($message, $context);
    }

//
//    public function info_record($message,$type='',array $context = []){
//        $this->useDailyFiles($type.'-info-record');
//        $this->log->write('info',$message,$context);
//    }
//
//    public function error_record($message,$type='',array $context = []){
//        $this->useDailyFiles($type.'-error-record');
//        $this->log->write('error',$message,$context);
//    }


    public function info($type = '', $message='', array $context = []){
        $type = empty($type)?__FUNCTION__:$type;
        $this->useDailyFiles($type);
        $this->instances[$type]->info($message,$context);

    }

//    public function debug($message,$type = '', array $context = []){
//        if(!empty($type)){
//            $this->useDailyFiles($type.'-'.__FUNCTION__);
//        }else{
//            $this->useDailyFiles(__FUNCTION__);
//        }
//        $this->log->write(__FUNCTION__,$message,$context);
//    }

    protected function prepareHandler(HandlerInterface $handler, array $config = [])
    {
        $isHandlerFormattable = false;

        if (Logger::API === 1) {
            $isHandlerFormattable = true;
        } elseif (Logger::API === 2 && $handler instanceof FormattableHandlerInterface) {
            $isHandlerFormattable = true;
        }

        if ($isHandlerFormattable && ! isset($config['formatter'])) {
            $handler->setFormatter($this->formatter());
        } elseif ($isHandlerFormattable && $config['formatter'] !== 'default') {
            $handler->setFormatter(app($config['formatter'], $config['formatter_with'] ?? []));
        }

        return $handler;
    }


    protected function formatter()
    {
        return tap(new LineFormatter(null, 'Y-m-d H:i:s', true, true), function ($formatter) {
            $formatter->includeStacktraces();
        });
    }

}