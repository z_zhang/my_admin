<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 20-1-14
 * Time: 下午5:05
 */

namespace App\Common;


use App\Service\Query\Admin\System\MenuQueryService;

class CheckMenu
{
    /**
     * 权限是否通过
     * @param $url,  路由
     * @return bool
     */
    public static function isPass($url)
    {
        if ( $url == 'upload' || $url == '/upload' ) {
            return true;    //上传权限直接通过
        }

        $auth = session('auth');

        $menuId = self::getUrlToId($url);

        if ( in_array($menuId,$auth) ) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUrlToId($url)
    {
        $menus = app(MenuQueryService::class)->getUrlToId();

        return $menus[$url]??0;
    }




}